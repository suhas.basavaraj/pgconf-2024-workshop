# pgconf-2024

## Name
Lab for __pgConf 2024 Training - PostgreSQL Healthcheck & Resiliency__


# Pre-Req needs to be followed by participates  
1. Docker service should be installed in your laptop/Desktop
2. Follow the steps from below links
    1. Ubuntu: https://docs.docker.com/engine/install/ubuntu/
    2. Windows: https://docs.docker.com/desktop/install/windows-install/
    3. Mac : https://docs.docker.com/desktop/install/mac-install/

## Description

>The Dockerized lab setup with Postgres 16 with replications setup.The container has pre built with below list of extensive tools :

* [PostgreSQL v16](https://www.postgresql.org/docs/16/index.html)
* [Python 3.11](https://www.python.org/downloads/release/python-3110/)
* [Perl 5.36](https://perldoc.perl.org/perl5360delta)
* __Sample Schema with Data__
* [Healthcheck script](https://gitlab.aws.dev/saship/postgres-health-check)
* [pg_repack](https://github.com/reorg/pg_repack)
* [hypopg](https://github.com/HypoPG/hypopg)
* [pgaudit](https://www.pgaudit.org/)
* [pl-profiler](https://github.com/bigsql/plprofiler)
* [pgBadger](https://github.com/darold/pgbadger)
* [pgBackrest](https://pgbackrest.org/)
* [pg_partman](https://github.com/pgpartman/pg_partman)
* [check_postgres.pl](https://bucardo.org/check_postgres/)
* [Standby with streaming relication](https://www.postgresql.org/docs/16/protocol-replication.html)

# Usage and steps to deploy containers

## Change to DOCKER directory
```
git clone https://gitlab.com/suhas.basavaraj/pgconf-2024-workshop.git
cd  pgconf-2024-workshop/DOCKER
```
### Build Postgres container image 

```
docker build -t pgconf_user/pgdatabase:latest -f dockerfile-postgres .
```
### Build plprofiler container image 

```
docker build -t pythonconfuser/plprofiler:latest -f dockerfile-plprofiler .
```

### Launch and create containers using docker compose 

#### Start master container and initialize with sample schema 


```
docker-compose up -d postgres_master
```

<!-- Adding Blockquote --> 
> $`\textcolor{red}{\text{IMPORTANT}}`$ After you start the master container, it will take atleast 5 minutes to load the data,ensure you start slave container once the master initialized completely
You can check the progress using below command 

```
docker-compose logs  postgres_master
```
<!-- Adding Blockquote --> 
> $`\textcolor{orange}{\text{NOTE}}`$ The logs should show something like below 
```
postgres_master  | INSERT 0 100000
postgres_master  | CREATE INDEX
postgres_master  | CREATE INDEX
postgres_master  | CREATE TABLE
postgres_master  | CREATE INDEX
postgres_master  | CREATE TABLE
postgres_master  | CREATE INDEX
postgres_master  |
postgres_master  |
postgres_master  | waiting for server to shut down....... done
postgres_master  | server stopped
postgres_master  |
postgres_master  | PostgreSQL init process complete; ready for start up.
postgres_master  |
postgres_master  | 2024-02-21 05:22:05 UTC [1]: [1-1] user=,db=,app=,client= LOG:  pgaudit extension initialized
postgres_master  | 2024-02-21 05:22:05 UTC [1]: [2-1] user=,db=,app=,client= LOG:  redirecting log output to logging collector process
postgres_master  | 2024-02-21 05:22:05 UTC [1]: [3-1] user=,db=,app=,client= HINT:  Future log output will appear in directory "log".
```

#### Start plprofiler container 
> $`\textcolor{Green}{\text{NOTE}}`$ This will create a container to run plrpofiler.All the containers connect with shared network bridge postgres_master 

```
docker-compose up -d plprofiler
```

#### Start slave container

> $`\textcolor{orange}{\text{NOTE}}`$ Make sure that master setup is completed as mentioned above before starting below slave setup

```
docker-compose up -d postgres_replica
```

#### Verify replication setup

```
docker exec -it postgres_master  bash

root@f294160b8ef3:/# psql -U postgres
psql (16.1 (Debian 16.1-1.pgdg120+1))
Type "help" for help.

postgres=# \x
Expanded display is on.

postgres=# select * from pg_stat_replication;
-[ RECORD 1 ]----+------------------------------
pid              | 48
usesysid         | 16385
usename          | replication_user
application_name | walreceiver
client_addr      | 192.168.224.4
client_hostname  |
client_port      | 49724
backend_start    | 2024-02-04 15:00:16.164147+00
backend_xmin     |
state            | streaming
sent_lsn         | 0/8B000148
write_lsn        | 0/8B000148
flush_lsn        | 0/8B000148
replay_lsn       | 0/8B000148
write_lag        |
flush_lag        |
replay_lag       |
sync_priority    | 0
sync_state       | async
reply_time       | 2024-02-04 15:19:36.671937+00
```

### Running plprofiler 
> $`\textcolor{Green}{\text{NOTE}}`$ Login to plprofiler container that can  connect Postgres container with shared network and below command

```
docker exec -it plprofiler  bash

export PGPASSWORD=password

[root@53cfaa8a49a6 /]# plprofiler run --command "select plp.inventory_in_stock(1525)" --output /tmp/inventory_in_stock.html -h postgres_master  -d pgconf -U postgres
select plp.inventory_in_stock(1525)
-- row1:
  inventory_in_stock: True
----
(1 rows)
SELECT 1 (0.015 seconds)

```

### Copy report from container to local 

```
docker cp plprofiler:/tmp/inventory_in_stock.html ~/Documents
```

### Generate health check report 

```
docker exec -it postgres_master   bash

 psql  -d pgconf  -U postgres -f /var/lib/scripts/pg_health_check.sql

 mv  pg_collector_pgconf-xxxx-xx-xx_xxxxxx.html /tmp/

 docker cp postgres_master:/tmp/pg_collector_pgconf-xxxx-xx-xx_xxxxxx.html   ~/Documents
```


## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.
1. __Suhas.B__(suhasraj@amazon.com)
2. __Rajesh.M__(madiwale@amazon.com)
3. __HariKrishna.B__(bkhari@amazon.com)

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
