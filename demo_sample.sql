chmod -R 777 /var/lib/scripts/
su - postgres
cd /var/lib/scripts/



healthcheck:

psql -d pgconf -f pg_health_check.sql 
cp *html /tmp/
docker cp pgdb_conf_master:/tmp/pg_collector_pgconf-2024-02-13_070011.html . 
open pg_collector_pgconf-2024-02-13_070011.html



Demo SQL-1 : work_mem
-----------------

pgbench -i -s 30 -d pgconf
psql -d pgconf

show work_mem;
explain analyze SELECT * FROM pgbench_accounts ORDER BY bid;
set work_mem = '512MB';
explain analyze SELECT * FROM pgbench_accounts ORDER BY bid;

Demo SQL-2 : datatype
-----------------

EXPLAIN ANALYZE UPDATE public.RNR_SEGMENT_PAX x SET ARC_EXPIRY_DATE = y.ARC_EXPIRY_DATE
       FROM (SELECT AIRLINE_IATA_CODE, PNR_NUMBER, ARC_EXPIRY_DATE, 0+row_num ROW_NUM
               FROM public.RNR_EXPIRY_DATE 
              WHERE airline_iata_code = 'XQ' 
                AND row_num BETWEEN (1*5000)+0 AND (1+1)*5000) y
         WHERE  x.airline_iata_code = y.airline_iata_code 
             AND  x.PNR_NUMBER =y.PNR_NUMBER;

alter table public.rnr_expiry_date alter column airline_iata_code type character varying(6) ;
analyze public.rnr_expiry_date;

EXPLAIN ANALYZE UPDATE public.RNR_SEGMENT_PAX x SET ARC_EXPIRY_DATE = y.ARC_EXPIRY_DATE
       FROM (SELECT AIRLINE_IATA_CODE, PNR_NUMBER, ARC_EXPIRY_DATE, 0+row_num ROW_NUM
               FROM public.RNR_EXPIRY_DATE 
              WHERE airline_iata_code = 'XQ' 
                AND row_num BETWEEN (1*5000)+0 AND (1+1)*5000) y
         WHERE  x.airline_iata_code = y.airline_iata_code 
             AND  x.PNR_NUMBER =y.PNR_NUMBER;


Demo SQL-3: IN Vs Exists
--------------------

EXPLAIN ANALYZE SELECT 
  TEST_VEH.TEST_VEH_ID, 
  TEST_VEH.VEHICLE_ID, 
  TEST_VEH.SERVICEPROGRAM_ID, 
  TEST_VEH.STARTDATE, 
  TEST_VEH.ENDDATE, 
  TEST_VEH.OILTYPE_ID 
FROM public.TEST_VEH TEST_VEH 
JOIN  public.OILTYPE OT ON  OT.OILTYPE_ID =TEST_VEH.OILTYPE_ID  
JOIN  public.SERVICEPROGRAM SP ON  SP.SERVICEPROGRAM_ID = TEST_VEH.SERVICEPROGRAM_ID
WHERE SP.PROGNAME  = '18FCE8FDAF365BB' 
  AND OT.OILTYPE_ID  =3
  AND TEST_VEH.ENDDATE  IS NOT NULL 
  AND TEST_VEH.TEST_VEH_ID NOT IN 
                      (SELECT TEST_VEH_ID
                              FROM public.VEHICLESERVICEHISTORY 
                              WHERE  TEST_VEH_ID > 1 
                      );





EXPLAIN ANALYZE SELECT 
  TEST_VEH.TEST_VEH_ID, 
  TEST_VEH.VEHICLE_ID, 
  TEST_VEH.SERVICEPROGRAM_ID, 
  TEST_VEH.STARTDATE, 
  TEST_VEH.ENDDATE, 
  TEST_VEH.OILTYPE_ID 
FROM public.TEST_VEH TEST_VEH 
JOIN  public.OILTYPE OT ON  OT.OILTYPE_ID =TEST_VEH.OILTYPE_ID  
JOIN  public.SERVICEPROGRAM SP ON  SP.SERVICEPROGRAM_ID = TEST_VEH.SERVICEPROGRAM_ID
WHERE SP.PROGNAME  = '18FCE8FDAF365BB' 
       AND OT.OILTYPE_ID  =3
       AND TEST_VEH.ENDDATE  IS NOT NULL  
       AND  NOT EXISTS 
                    (SELECT TEST_VEH_ID 
                            FROM public.VEHICLESERVICEHISTORY 
                            WHERE TEST_VEH.TEST_VEH_ID=VEHICLESERVICEHISTORY.TEST_VEH_ID 
                            AND TEST_VEH_ID > 1 
                    );

Function call in where condition:
----------------------------------

explain analyze select * from public.rnr_expiry_date where pnr_number= 'EE9F41';
explain analyze select * from public.rnr_expiry_date where pnr_number= public.return_data();
explain analyze select * from public.rnr_expiry_date where pnr_number= (select public.return_data() );



Demo SQL-4 pgpartman
----------------

SSELECT create_parent( p_parent_table => 'public.partition_table',
 p_control => 'id',
 p_interval=> '1000',
 p_premake => 10);


insert into public.partition_table values(generate_series(1,100000),'aws',now(),'pgconf 2024');
insert into public.nonpartition_table values(generate_series(1,100000),'aws',now(),'pgconf 2024');


explain analyze select * from public.partition_table where  id between 10 and 22;
explain analyze select * from public.nonpartition_table where  id between 10 and 22;

CALL run_maintenance_proc();

Demo SQL-5 Analyze
------------------

explain select a from t1 where b = 5;
explain (analyze,buffers,costs off) select a from t1 where b = 5;
Analyze t1;
explain (analyze,buffers,costs off) select a from t1 where b = 5;
update t1 set a=8 where b=5;
explain (analyze,buffers,costs off) select a from t1 where b = 5;


Demo SQL-6 Vacuum
-------------------

insert into t1 values (generate_series(1,100000),generate_series(1,100000),generate_series(1,100000));
delete from t1 where a between 200 and 10000;
vacuum analyze t1;

select * from pgtoolkit.table_bloat where tablename='t1';
vacuum analyze t1;

insert into t1 values (generate_series(100000,200000),generate_series(100000,200000),generate_series(100000,200000));
vacuum analyze t1;
select * from pgtoolkit.table_bloat where tablename='t1';

Demo SQL-7 index bloat
----------------------
select * from pgtoolkit.index_bloat where tblname='index_bloat_1';

update index_bloat_1  set salary = '3000' where id between 2000 and 8000;
analyze index_bloat_1 ;
select * from pgtoolkit.index_bloat where tblname='index_bloat_1';

reindex index concurrently index_bloat_2_idx;
select * from pgtoolkit.index_bloat where tblname='index_bloat_1';
vacuum verbose index_bloat_1;


Demo-8 pg_repack
----------------

pgbench -j 6 -T 30 -n -c 10 -d pgconf
select * from pgtoolkit.table_bloat;
analyze pgbench_tellers,pgbench_branches
pg_repack -d pgconf -t pgbench_tellers -t pgbench_branches





Demo-8 plprofiler
------------------

docker exec -it plprofiler  bash
export PGPASSWORD=password
plprofiler run --command "select plp.inventory_in_stock(1525)" --output /tmp/inventory_in_stock.html -h postgres_master  -d pgconf -U postgres
docker cp plprofiler:/tmp/inventory_in_stock.html .

explain analyze SELECT 1 FROM plp.rental WHERE rental.inventory_id =1525;
CREATE INDEX idx_rental_inventory_id ON plp.rental USING btree(inventory_id);

export PGPASSWORD=password
plprofiler run --command "select plp.inventory_in_stock(1525)" --output /tmp/inventory_in_stock.html -h postgres_master  -d pgconf -U postgres
docker cp plprofiler:/tmp/inventory_in_stock.html .


Demo-9 pgbadger
--------------
pgbadger -i postgres*.log -o /tmp/postgres-pgconf-pgbadger.html
docker cp pgdb_conf:/tmp/postgres-pgconf-pgbadger.html . 
open postgres-pgconf-pgbadger.html



Demo-10 Custom Monitor
----------------------

tar -xvf check_postgres-2.26.0.tar.gz
cd check_postgres-2.26.0
perl5.36.0 check_postgres.pl --symlinks
./check_postgres_bloat --dbname=pgconf --port=5432 --warning='100 M' --critical='200 M'
./check_postgres_wal_files --dbname=pgconf --port=5432 --warning='100' --critical='200'
./check_postgres_txn_wraparound --dbname=pgconf --port=5432
./check_postgres_database_size --dbname=pgconf --port=5432 --warning='1G' --critical='4G'
./check_postgres_locks --dbname=pgconf --port=5432


pgaudit
--------

set pgaudit.log to 'write,read, ddl'
set pgaudit.log_relation to 'on'

create table account
(
    id int,
    name text,
    password text,
    description text
);

insert into account (id, name, password, description)
             values (1, 'user1', 'HASH1', 'blah, blah');

select * from account;

cd log
ls -lrth
cat lastfile.log
