#!/usr/bin/env bash

cat /var/lib/config/postgresql.conf > /var/lib/postgresql/data/postgresql.conf
cat /var/lib/config/pg_hba.conf > /var/lib/postgresql/data/pg_hba.conf
