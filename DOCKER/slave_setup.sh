#!/bin/bash

sleep 15
pg_ctl -D /var/lib/postgresql/data stop -mf
sleep 5
rm -rf /var/lib/postgresql/data/*
until pg_basebackup --pgdata=/var/lib/postgresql/data -R --slot=repl_slot --host=postgres_master --port=5432 -X stream -U replication_user; do
    echo 'Waiting for primary to connect...'
    sleep 5
done
echo 'Backup done, starting replica...'
chmod 0700 /var/lib/postgresql/data
pg_ctl -D /var/lib/postgresql/data start -mf
