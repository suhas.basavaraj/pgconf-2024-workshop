-- +-----------------------------------------------------------------------------------+
-- | Decription:
-- |   - This script is bulit on top of pg_collector
-- |   - This script is used to generate the PostgreSQL database Health Check recommendations
-- |
-- | Supported Database Version:
-- |   - This script is tested on PG 12.x, PG 13.x, PG 14.x
-- |
-- | Prerequisites:
-- |   - pg_stat_statements extension and configuration. This is not a mandatory requirement for the report. If extension is not available, a Notice is displayed on Top of report.
-- |   - psql client
-- |
-- | How to run:
-- | psql -h <>host -d <db name> -U <user name> -f <path to helathcheck script>/pg_health_check.sql
-- +-----------------------------------------------------------------------------------+


-- +----------------------------------------------------------------------------+
-- |      - Server Version                                   -                  |
\o /dev/null

select regexp_replace(current_setting('server_version')::text,'\..*','') as db_major_version
from pg_settings where name = 'server_version';
\gset

select (:'db_major_version')::integer as db_major_version;
\gset


select (9 = :'db_major_version') as is_eq_9; 
\gset

select (10 = :'db_major_version') as is_eq_10; 
\gset

select (11 = :'db_major_version') as is_eq_11; 
\gset

select (12 = :'db_major_version') as is_eq_12; 
\gset

select (13 = :'db_major_version') as is_eq_13; 
\gset

select (14 = :'db_major_version') as is_eq_14; 
\gset

select (15 = :'db_major_version') as is_eq_15; 
\gset

select (13 <= :'db_major_version') as is_ge_13;
\gset



-- Checking if pg_stat_statements is installed or not
select max(extver) as pg_stat_stmts_ext_ver from (
select extversion::numeric as extver from pg_extension where extname = 'pg_stat_statements'
union all
select 0
) a;
\gset

select :pg_stat_stmts_ext_ver as pg_stat_stmts_ext_version;
\gset

select (0 < :pg_stat_stmts_ext_version) as pg_stat_stmts_is_available; 
\gset


-- +----------------------------------------------------------------------------+

\H
\set filename :DBNAME-`date +%Y-%m-%d_%H%M%S`
\echo Report name and location: pg_healthcheck_:filename.html
\o pg_collector_:filename.html
\pset footer  off
\qecho <style type='text/css'> 
\qecho body { 
\qecho font:10pt Arial,Helvetica,sans-serif;
\qecho color:Black Russian; background:white; } 
\qecho p { 
\qecho font:10pt Arial,sans-serif;
\qecho color:Black Russian; background:white; } 
\qecho table,tr,td { 
\qecho font:10pt Arial,Helvetica,sans-serif; 
\qecho text-align:center; 
\qecho color:Black Russian; background:white; 
\qecho padding:0px 0px 0px 0px; margin:0px 0px 0px 0px; } 
\qecho th { 
\qecho font:bold 10pt Arial,Helvetica,sans-serif; 
\qecho color:#16191f; 
\qecho background:#e59003; 
\qecho padding:0px 0px 0px 0px;} 
\qecho h1 { 
\qecho font:bold 16pt Arial,Helvetica,Geneva,sans-serif; 
\qecho color:#16191f; 
\qecho background-color:#e59003; 
\qecho border-bottom:1px solid #e59003;
\qecho text-align: center;
\qecho margin-top:0pt; margin-bottom:0pt; padding:20px 20px 20px 20px;}
\qecho h2 {
\qecho font:bold 10pt Arial,Helvetica,Geneva,sans-serif;
\qecho color:#16191f; 
\qecho background-color:White; 
\qecho margin-top:4pt; margin-bottom:0pt;}
\qecho h3 {
\qecho font:bold 10pt Arial,Helvetica,Geneva,sans-serif;
\qecho color:#16191f;
\qecho background-color:White;
\qecho margin-top:4pt; margin-bottom:0pt;} 
\qecho a { 
\qecho font:9pt Arial,Helvetica,sans-serif; 
\qecho color:#663300; 
\qecho background:#ffffff; 
\qecho margin-top:0pt; margin-bottom:0pt; vertical-align:top;} 
\qecho .threshold-critical { 
\qecho font:bold 10pt Arial,Helvetica,sans-serif; 
\qecho color:red; } 
\qecho .threshold-warning { 
\qecho font:bold 10pt Arial,Helvetica,sans-serif; 
\qecho color:orange; } 
\qecho .threshold-ok { 
\qecho font:bold 10pt Arial,Helvetica,sans-serif; 
\qecho color:green; } 
\qecho .header img {
\qecho  float: left;
\qecho  width: 120px;
\qecho  height: 50px;
\qecho  padding: 6px;
\qecho  }
\qecho  ol {
\qecho    counter-reset: item
\qecho  }
\qecho  li {
\qecho    display: block
\qecho  }
\qecho  li:before {
\qecho    content: counters(item, ".") " ";
\qecho    counter-increment: item
\qecho  }
\qecho </style> 
\qecho <br>
\qecho <div class="header"> 
\qecho <div class="image"><a href="https://aws.amazon.com/what-is-cloud-computing"><img src="https://d0.awsstatic.com/logos/powered-by-aws.png" alt="Powered by AWS Cloud Computing"></a></div>
\qecho <div class="text"><h1 align="center" style="background-color:#9fc5e8" >PostgreSQL Health Check Report</h1></div>
\qecho </div>
--\qecho <center><img src="/Users/saship/Documents/MY_Tools/PG_Healthcheck/V2/AWS-logo-2.jpg" alt="aws_logo" width="120" height="75" /></center>
\qecho <br>
\if :pg_stat_stmts_is_available
  \qecho
\else
  \qecho <p style="color:red"><strong>Warning: "pg_stat_statements" extension is not installed in the current database. "pg_stat_statements extension" section will not be populated.</strong></p>
\endif

\qecho <br> <br>
\qecho <div class="table-content">
\qecho <p style="font-family:verdana"><strong>Table of Contents</strong></p>
\qecho <ol>
\qecho <li>Database Info
    \qecho <ol>
    \qecho <li><a href="#Environment">Environment</a></li>
    \qecho <li><a href="#List_of_Databases">List of Databases</a></li>
    \qecho <li><a href="#DB_parameters">Database parameters</a></li>
    \qecho <li><a href="#Extensions">Database Extensions</a></li>
    \qecho <li><a href="#DB_Load">Database Load</a></td>
    \qecho <li><a href="#Schema_Info">Schema Info</a></td>
    \qecho <li><a href="#Users_Roles_Info">Users & Roles Info</a></td>
    \qecho </ol>
    \qecho <br>
\qecho <li>Table & Index Info
    \qecho <ol>
    \qecho <li><a href="#Table_Size">Table Size</a></td> 
    \qecho <li><a href="#index_Size">Index Size</a></td>
    \qecho <li><a href="#Unused_Indexes">Unused Indexes</a></td> 
    \qecho <li><a href="#Duplicate_indexes">Duplicate indexes</a></td>
    \qecho <li><a href="#Index_Access_Profile">Index Access Profile</a></td>
    \qecho <li><a href="#PK_FK_using_numeric_or_integer_data_type">PK or FK using numeric data type</a></td>
    \qecho <li><a href="#invalid_indexes">Invalid indexes</a></td>
    \qecho <li><a href="#unlogged_tables">Unlogged Tables</a></td>
    \qecho <li><a href="#Temp_tables">Temp tables</a></td>
    \qecho <li><a href="#Partition_tables">Partition tables</a></td>
    \qecho <li><a href="#FK_without_index">FK without index</a></td>
    \qecho <li><a href="#Table_Access_Profile">Table Access Profile</a></td> 
    \qecho <li><a href="#TablesWithNoPK">Tables with No PK</a></li>
    \qecho <li><a href="#LongRunningSql">Long running sql (more than 10 minutes)</a></li>
    \qecho </ol>   
    \qecho <br>
\qecho <li>Vacuum/Auto Vacuum info
    \qecho <ol>
    \qecho <li><a href="#Transaction_ID_TXID">Transaction ID TXID (Wraparound)</a></td> 
    \qecho <li><a href="#vacuum_Statistics">Vacuum & Statistics</a></td>
    \qecho <li><a href="#Fragmentation">Fragmentation (Bloat)</a></td>
    \qecho </ol>
    \qecho <br>
\qecho <li>Others
    \qecho <ol>
    \qecho <li><a href="#Memory_setting">Memory setting</a></td>
    \qecho <li><a href="#pg_stat_statements_extension">pg_stat_statements extension</a></td>
    \qecho <li><a href="#Replication">Replication</a></td>
    \qecho <li><a href="#CurrentTemporaryFileUsage">Current Temporary File Usage</a></td>
    \qecho <li><a href="#Toast_Tables_Mapping">Toast Tables Mapping</a></td>
    \qecho <li><a href="#sessions_info">Sessions/Connections Info</a></td>
    \qecho <li><a href="#Orphaned_prepared_transactions">Orphaned prepare transactions</a></td>
    \qecho <li><a href="#pgaudit_extension">pgaudit extension</a></td>
    \qecho <li><a href="#access_privileges">Access privileges</a></td>
    \qecho <li><a href="#ssl">ssl</a></td>
    \qecho <li><a href="#background_processes">Background processes</a></td>
    \qecho <li><a href="#Multixact_ID_MXID">Multixact ID MXID (Wraparound)</a></td> 
    \qecho <li><a href="#Large_objects">Large objects</a></td>
    \qecho <li><a href="#pg_shdepend">pg_shdepend</a></td>
    \qecho <li><a href="#sequences">Sequences</a></td>
    \qecho <li><a href="#functions_statistics">Functions statistics</a></td> 
    \qecho <li><a href="#triggers">Triggers</a></td>
    \qecho <li><a href="#pg_hba.conf">pg_hba.conf</a></td>
    \qecho </ol>
    \qecho <br>
\qecho </li>
\qecho </div> 
\qecho <br><br>


-- +----------------------------------------------------------------------------+
-- |      - Database Info                                    -                  |
-- +----------------------------------------------------------------------------+

\set QUIET 1
select case when count(*)=0 then 'select ''not-aurora'' as avers'
                            else 'select aurora_version() as avers'
       end as aurora_version_query
from pg_settings where name='rds.extensions' and setting like '%aurora_stat_utils%' \gset

prepare detect_aurora as :aurora_version_query;

execute detect_aurora \gset
--deallocate detect_aurora;
with
  pgvers as (
    select current_setting('server_version') as v
  ), allvers as (
    select 1 priority, 'Aurora-'||v||'-'||:'avers' as version from pgvers
    where :'avers' <> 'not-aurora'
      union all
    select 2, 'RDS-'||v from pgvers, pg_settings s
    where s.name like 'rds.%'
      union all
    select 3, 'pg-'||v from pgvers
  )
select first_value(version) over (order by priority) as server_version
from allvers limit 1 \gset

\qecho <a name="Environment"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Environment</b></font><hr align="left" width="150">
\set v_hostname :HOST
SELECT 'Is RDS/Aurora'                  as "Name", :'avers' as "Value" UNION ALL
SELECT 'Database Host/RDS Endpoint'     as "Name", :'HOST' as "Value" UNION ALL
SELECT 'PostgreSQL Database Name' as Name, current_database() as Value UNION ALL
SELECT 'PostgreSQL Version'       as Name, substring(version()::text FROM '(^.*) on') UNION ALL
SELECT 'PostgreSQL Type'          as Name, CASE WHEN :'HOST' ~ '.\.rds\.amazonaws\.com$' THEN 'RDS' ELSE 'Non-RDS' END as Value UNION ALL
SELECT 'Database Role'            as Name, case when pg_is_in_recovery() then 'Standby/Reader DB (Read Only)' else 'Primary/writer DB (Read write)' end as standby_mode UNION ALL
SELECT 'Current Database Time'    as Name, date_trunc('second', clock_timestamp()::timestamp)::text UNION ALL
SELECT 'Database Startup Time'    as Name, date_trunc('second', pg_postmaster_start_time()::timestamp)::text as Value UNION ALL
SELECT 'Database running since'   as Name, date_trunc('second', current_timestamp - pg_postmaster_start_time()::timestamp)::text as Value UNION ALL
SELECT 'Total Active Sessions'    as Name, count(*)::text as Value FROM pg_stat_activity where state ='active'  UNION ALL
SELECT 'Total No. of Sessions'    as Name, count(*)::text as Value FROM pg_stat_activity;

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>

-- +----------------------------------------------------------------------------+
-- |      - List of Databases                                    -              |
-- +----------------------------------------------------------------------------+

\qecho <a name="List_of_Databases"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>List of Databases</b></font><hr align="left" width="150">
\l+

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>


-- +----------------------------------------------------------------------------+
-- |      - DB Parameters                                    -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="DB_parameters"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Database Parameters</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details> This section only shows selected PostgreSQL parameters. If you want more details on RDS PostgreSQL parameters, you can refer article <a href="https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.PostgreSQL.CommonDBATasks.Parameters.html" target="_blank">Working with parameters on your RDS for PostgreSQL</a>
\qecho </details> <br>

SELECT name       as "Database Parameter", 
       setting    as "Current Value",
       unit       as "Unit",
       source     as "Source",
       short_desc as "Short Description"
  FROM pg_settings
 WHERE name in (
               'max_connections',
               'idle_in_transaction_session_timeout',
               'shared_preload_libraries',
               'track_activity_query_size',
               'rds.force_admin_logging_level',
               'rds.force_autovacuum_logging_level',
               'rds.logical_replication',
               'rds.force_ssl',
               'log_line_prefix',
               --Autovacuum
               'autovacuum',
               'rds.adaptive_autovacuum',
               'autovacuum_analyze_scale_factor',
               'autovacuum_analyze_threshold',
               'autovacuum_vacuum_scale_factor',
               'autovacuum_vacuum_threshold',
               'autovacuum_naptime',
               'autovacuum_max_workers',
               'autovacuum_vacuum_cost_delay',
               'autovacuum_vacuum_cost_limit',
               'autovacuum_vacuum_insert_scale_factor',
               'autovacuum_vacuum_insert_threshold',
               'autovacuum_work_mem',
               'vacuum_freeze_min_age',
               'vacuum_cost_delay',
               'vacuum_cost_limit',
               'vacuum_cost_page_dirty',
               'vacuum_cost_page_hit',
               'vacuum_cost_page_miss',
               'vacuum_defer_cleanup_age',
               --Memory & Performance
               'effective_cache_size',
               'wal_buffers',
               'temp_buffers',
               'shared_buffers',
               'work_mem',
               'maintenance_work_mem',
               'huge_pages',
               'random_page_cost',
               --Replication
               'wal_level',
               'max_wal_senders',
               'max_replication_slots',
               'max_worker_processes',
               'max_logical_replication_workers',
               'wal_sender_timeout',
               'wal_receiver_timeout',
               'max_sync_workers_per_subscription',
               'wal_receiver_status_interval',
               'wal_retrieve_retry_interval',
               --Logging for pgbadger
               'log_connections',
               'log_disconnections',
               'log_autovacuum_min_duration',
               'log_min_duration_statement',
               'log_temp_files',
               'log_lock_waits',
               'rds.log_retention_period'
               )
order by 1;


\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Database Extensions                              -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Extensions"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Extensions</b></font><hr align="left" width="460">
\qecho <br>
\qecho <h3>Installed extensions :  </h3>
SELECT e.extname AS "Extension Name", e.extversion AS "Version", n.nspname AS "Schema",pg_get_userbyid(e.extowner)  as Owner,  c.description AS "Description" , e.extrelocatable as "relocatable to another schema", e.extconfig ,e.extcondition
FROM pg_catalog.pg_extension e LEFT JOIN pg_catalog.pg_namespace n ON n.oid = e.extnamespace LEFT JOIN pg_catalog.pg_description c ON c.objoid = e.oid AND c.classoid = 'pg_catalog.pg_extension'::pg_catalog.regclass
ORDER BY 1;
\qecho <br>
\qecho <h3>Installed Extensions to upgrade : </h3>
select name as extension_name , max(version) as latest_version
from
(select b.name , b.version ,b.installed
from
(SELECT extname ,extversion FROM pg_extension) a ,
(SELECT name ,version ,installed FROM pg_available_extension_versions where name in (SELECT extname FROM pg_extension)) b
where a.extname = b.name
and b.version > a.extversion
order by b.name , b.version ) e
group by name;
\qecho <br>
\qecho <details>
\qecho <p> Extensions expand on the functionality provided by the PostgreSQL engine. You can find a list of extensions supported by Amazon RDS in the default DB parameter group for that PostgreSQL version. </p>
--select * from pg_available_extension_versions order by name,version;
--select * from pg_available_extensions order by installed_version;
\qecho <a href="https://docs.aws.amazon.com/AmazonRDS/latest/PostgreSQLReleaseNotes/postgresql-extensions.html" target="_blank">Extension versions for Amazon RDS for PostgreSQL</a>
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - DB_Load                                          -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="DB_Load"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Database Load</b></font><hr align="left" width="460">
\qecho <br>
\qecho <h3>How many session waiting on CPU and None CPU wait event:</h3>
\qecho <details>
select coalesce(count(*),'0') as  count_of_sessions_waiting_on_CPU
FROM pg_stat_activity 
where wait_event is null and state = 'active' group by wait_event ;
\qecho <br>
select coalesce(sum(count),'0') as count_of_sessions_waiting_on_None_CPU
from (SELECT count(*) as count
FROM pg_stat_activity  
where wait_event is not null and state = 'active' 
group by wait_event) as c;
\qecho </details>

\qecho <br>
\qecho <h3>wait events:</h3>
\qecho <details>
\qecho <h3>wait events/session count :</h3>
SELECT coalesce(wait_event,'CPU') as wait_event , count(*) FROM pg_stat_activity group by wait_event order by 2 desc;
\qecho <br>
\qecho <h3>wait events/query  :</h3>
SELECT  coalesce(wait_event,'CPU') as wait_event, substr(query,1,150) as query,count(*) FROM pg_stat_activity   group by  query,wait_event order by 3 desc;
\qecho <br>
\qecho <h3>wait events/user name :</h3>
SELECT coalesce(wait_event,'CPU') wait_event,usename as user_name, count(*) FROM pg_stat_activity group by wait_event, usename order by 3 desc ;
\qecho </details>


\qecho <br>
\qecho <h3>Lock :</h3>
\qecho <details>
\qecho <br>
\qecho <h3>Not granted lock :</h3>
SELECT coalesce(count(*),0) as "not_granted_lock" FROM pg_locks WHERE NOT GRANTED;
\qecho <br>
\qecho <h3>blocked sessions :</h3>
select count(*) from pg_stat_activity where cardinality(pg_blocking_pids(pid)) > 0 ;
\qecho <br>
\qecho <h3>lock_mode :</h3>
SELECT mode as lock_mode , count(*) FROM pg_locks group by mode;
\qecho <br>
\qecho <h3>lock_type :</h3>
SELECT locktype as lock_type , count(*) FROM pg_locks group by locktype;
\qecho </details>


\qecho <br>
\qecho <h3>pg_stat_* views:</h3>
\qecho <details>
\qecho <br>
\qecho <h3>pg_stat_bgwriter view:</h3>
select * from pg_stat_bgwriter;
\qecho <br>
\qecho <h3>pg_stat_database view:</h3>
select * from pg_stat_database;
\qecho <br>
\qecho <h3>pg_stat_database_conflicts view:</h3>
select * from pg_stat_database_conflicts;
\qecho </details>


\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Schema_Info                                      -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Schema_Info"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Schema Info</b></font><hr align="left" width="460">
\qecho <br>
SELECT schema_name,
       pg_size_pretty(sum(table_size)::bigint) schema_size,
       round((sum(table_size) / pg_database_size(current_database())) * 100, 2) "Size_of_db"
FROM (
  SELECT pg_catalog.pg_namespace.nspname as schema_name,
         pg_relation_size(pg_catalog.pg_class.oid) as table_size
  FROM   pg_catalog.pg_class
     JOIN pg_catalog.pg_namespace ON relnamespace = pg_catalog.pg_namespace.oid
) t
GROUP BY schema_name
ORDER BY sum(table_size)::bigint desc;

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Users_Roles_Info                                 -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Users_Roles_Info"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Users & Roles Info</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\du
\qecho <br>
-- list of per database role settings (settings set at the role level)
\drds
\qecho <br>
select * FROM pg_user;
\qecho </details>


\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- +----------------------------------------------------------------------------+
-- |      2 Table & Index Info                               -                  |
-- +----------------------------------------------------------------------------+
-- +----------------------------------------------------------------------------+


-- +----------------------------------------------------------------------------+
-- |      - Table_Size                                   -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Table_Size"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Table Size</b></font><hr align="left" width="460">
\qecho <br>
\qecho <h3>Biggest 50 tables in the DB order by size: </h3>
\qecho <br>
\qecho <details>
SELECT *, pg_size_pretty(total_bytes) AS TOTAL_PRETTY
    , pg_size_pretty(index_bytes) AS INDEX_PRETTY
    , pg_size_pretty(toast_bytes) AS TOAST_PRETTY
    , pg_size_pretty(table_bytes) AS TABLE_PRETTY
  FROM (
  SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS TABLE_BYTES FROM (
      SELECT c.oid,nspname AS table_schema, relname AS TABLE_NAME
              , c.reltuples::bigint AS ROW_ESTIMATE
              , pg_total_relation_size(c.oid) AS TOTAL_BYTES
              , pg_indexes_size(c.oid) AS INDEX_BYTES
              , pg_total_relation_size(reltoastrelid) AS TOAST_BYTES
          FROM pg_class c
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE relkind = 'r'
  ) a
) a
order by total_bytes desc, table_schema, table_name
LIMIT 50;
\qecho </details>
\qecho <br>
\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - index_Size                                    -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="index_Size"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Index Size</b></font><hr align="left" width="460">
\qecho <br>
\qecho <h3>Biggest 50 Index in the DB order by size:</h3>
\qecho <br>
\qecho <details>
SELECT
schemaname,relname as table_name,
indexrelname AS indexname,
pg_relation_size(indexrelid) AS index_size_bytes,
pg_size_pretty(pg_relation_size(indexrelid)) AS index_size
FROM pg_catalog.pg_statio_all_indexes  
ORDER BY index_size_bytes desc, schemaname, table_name
LIMIT 50;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>


-- +----------------------------------------------------------------------------+
-- |      - Unused_Indexes                                   -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Unused_Indexes"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Unused Indexes</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
SELECT ai.schemaname,ai.relname AS tablename,ai.indexrelid  as index_oid ,
ai.indexrelname AS indexname,i.indisunique ,
ai.idx_scan ,
pg_relation_size(ai.indexrelid) as index_size,
pg_size_pretty(pg_relation_size(ai.indexrelid)) AS pretty_index_size
FROM pg_catalog.pg_stat_all_indexes ai , pg_index i
WHERE ai.indexrelid=i.indexrelid
and ai.idx_scan = 0 
and ai.schemaname not in ('pg_catalog', 'pg_toast')
order by index_size desc;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Duplicate_indexes                                   -               |
-- +----------------------------------------------------------------------------+

\qecho <a name="Duplicate_indexes"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Duplicate indexes</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
/*
SELECT pg_size_pretty(sum(pg_relation_size(idx))::bigint) as size,
       (array_agg(idx))[1] as idx1, (array_agg(idx))[2] as idx2,
       (array_agg(idx))[3] as idx3, (array_agg(idx))[4] as idx4
FROM (
    SELECT indexrelid::regclass as idx, (indrelid::text ||E'\n'|| indclass::text ||E'\n'|| indkey::text ||E'\n'||
                                         coalesce(indexprs::text,'')||E'\n' || coalesce(indpred::text,'')) as key
    FROM pg_index) sub
GROUP BY key HAVING count(*)>1
ORDER BY sum(pg_relation_size(idx)) DESC;
*/
SELECT
    ix.schemaname,
    ix.tablename,
    unnest(array_agg(indexname)) AS Index,
    unnest(array_agg(indexdef)) AS Index_def,
    pg_size_pretty(sum(pg_relation_size(i.indexrelid::regclass))::bigint) as size
FROM pg_indexes ix
INNER JOIN pg_index i 
		ON i.indrelid::regclass::text = ix.tablename 
	   AND i.indexrelid::regclass::text = ix.indexname
WHERE ix.schemaname not in('pg_catalog')
GROUP BY ix.schemaname, ix.tablename
HAVING COUNT(*) > 1
ORDER BY 1,2,3;

\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Index_Access_Profile                                    -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Index_Access_Profile"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Index Access Profile</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
with index_size_info as 
(
SELECT
schemaname,relname as "Table",
indexrelname AS indexname,
indexrelid,
pg_relation_size(indexrelid) index_size_byte,
pg_size_pretty(pg_relation_size(indexrelid)) AS index_size
FROM pg_catalog.pg_statio_all_indexes  ORDER BY 1,4 desc) 
Select a.schemaname, 
a.relname as "Table_Name",
a.indexrelname AS indexname,
b.index_size,
a.idx_scan,
a.idx_tup_read,
a.idx_tup_fetch
from pg_stat_all_indexes a ,  index_size_info b
where a.idx_scan >0  
and a.indexrelid=b.indexrelid
and a.schemaname not in ('pg_catalog')
order by b.index_size_byte desc,a.idx_scan asc ;
\qecho </details>
\qecho <br> 
\qecho <h4> pg_statio_all_indexes  View : </h4>
\qecho <h4> physical reads (disk blocks read or Reads from Disk) = idx_blks_read  </h4>
\qecho <h4> logical reads (buffer hits or Read from Memory) = idx_blks_hit  </h4>
\qecho <br>
\qecho <h3> Top 50 index by physical reads : </h3>
\qecho <br> 
\qecho <details>
select
schemaname        as schema_name  ,
relname            as table_name     ,
indexrelname    as index_name,
coalesce(idx_blks_read,0)   as indexe_disk_blocks_read,
coalesce(idx_blks_hit,0)    as indexe_buffer_hits    ,
coalesce(trunc((coalesce(idx_blks_read,0)
/ 
NULLIF(
coalesce(idx_blks_read,0)
+coalesce(idx_blks_hit,0)
,0) ) * 100,2),0) as physical_reads_percent ,
coalesce(trunc((coalesce(idx_blks_hit,0)
/ 
NULLIF(
coalesce(idx_blks_read,0)
+coalesce(idx_blks_hit,0)
,0) ) * 100,2),0) as logical_reads_percent
from 
pg_statio_all_indexes 
where schemaname not in ('pg_toast','pg_catalog','information_schema')
order by indexe_disk_blocks_read desc limit 50 ;
\qecho </details>
\qecho <br>
\qecho <h3> Top 50 index by physical reads percent  : </h3>
\qecho <br> 
\qecho <details>
select
schemaname        as schema_name  ,
relname            as table_name     ,
indexrelname    as index_name,
coalesce(idx_blks_read,0)   as indexe_disk_blocks_read,
coalesce(idx_blks_hit,0)    as indexe_buffer_hits    ,
coalesce(trunc((coalesce(idx_blks_read,0)
/ 
NULLIF(
coalesce(idx_blks_read,0)
+coalesce(idx_blks_hit,0)
,0) ) * 100,2),0) as physical_reads_percent ,
coalesce(trunc((coalesce(idx_blks_hit,0)
/ 
NULLIF(
coalesce(idx_blks_read,0)
+coalesce(idx_blks_hit,0)
,0) ) * 100,2),0) as logical_reads_percent
from 
pg_statio_all_indexes 
where schemaname not in ('pg_toast','pg_catalog','information_schema')
order by physical_reads_percent desc limit 50 ;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - PK_FK_using_numeric_or_integer_data_type         -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="PK_FK_using_numeric_or_integer_data_type"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>PK or FK using numeric data type</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
select kcu.table_schema,
       kcu.table_name,
       tco.constraint_name,
       tco.constraint_type,
       kcu.column_name as column_name,
       cols.data_type
from information_schema.columns cols
join information_schema.key_column_usage kcu 
     on  cols.table_schema = kcu.table_schema
     and cols.table_name = kcu.table_name
     and cols.column_name = kcu.column_name
join information_schema.table_constraints tco
     on kcu.constraint_schema = tco.constraint_schema
     and kcu.constraint_name = tco.constraint_name
     and kcu.table_schema = tco.table_schema
     and kcu.table_name = tco.table_name
where tco.constraint_type in ('PRIMARY KEY', 'FOREIGN KEY')
  and cols.data_type = 'numeric'
order by kcu.table_schema, kcu.table_name, kcu.ordinal_position;

\qecho </details>


\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>




-- +----------------------------------------------------------------------------+
-- |      - invalid_indexes                                 -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="invalid_indexes"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Invalid indexes</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
select count (*) as count_of_invalid_indxes from pg_index WHERE pg_index.indisvalid = false ;
with table_info as 
(SELECT pg_index.indrelid , pg_class.oid, pg_class.relname as table_name 
from   pg_class , pg_index
where pg_index.indrelid = pg_class.oid )
SELECT distinct pg_index.indexrelid as INDX_ID,pg_class.relname as index_name ,table_info.table_name,pg_namespace.nspname as schema_name  , pg_class.relowner as owner_id , pg_index.indisvalid as indx_is_valid
FROM pg_class , pg_index ,pg_namespace , table_info
WHERE pg_index.indisvalid = false 
AND pg_index.indexrelid = pg_class.oid
and pg_class.relnamespace = pg_namespace.oid
and pg_index.indrelid = table_info.oid
;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - unlogged_tables                                    -                |
-- +----------------------------------------------------------------------------+

\qecho <a name="unlogged_tables"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Unlogged Tables</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho <h3>Number of Unlogged Tables : </h3>
select count (*) FROM pg_class WHERE relpersistence = 'u';
\qecho <br>
select relname as table_name, relpersistence FROM pg_class WHERE relpersistence = 'u';
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Temp tables                                      -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Temp_tables"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Temp tables</b></font><hr align="left" width="460">

\qecho <br>
\qecho <details>
\qecho <h3>Parameters:</h3>

select 
name as parameter_name,setting,unit,short_desc  
FROM pg_catalog.pg_settings 
WHERE name in ('temp_tablespaces','temp_file_limit','log_temp_files' ) ;
\qecho <br>
select name as parameter_name, setting , unit,   (((setting::BIGINT)*8)/1024)::BIGINT  as "size_MB" ,(((setting::BIGINT)*8)/1024/1024)::BIGINT  as "size_GB", pg_size_pretty((((setting::BIGINT)*8)*1024)::BIGINT),short_desc  
from pg_settings where name in ('temp_buffers') ;
\qecho <br>
\qecho <h3>Temp tables statistics:</h3>
\qecho <h4>Note: Number of temporary files created by queries in every Database and total amount of data written to temporary files by queries in every Database </h4>
\qecho <br>
select datname as database_name, temp_bytes/1024/1024 temp_size_MB,
temp_bytes/1024/1024/1024 temp_size_GB ,temp_files  from  pg_stat_database
where  temp_bytes + temp_files > 0
and datname is not null  
order by 2  desc;

\qecho <br>
SELECT
n.nspname as SchemaName
,c.relname as RelationName
,CASE c.relkind
WHEN 'r' THEN 'table'
WHEN 'v' THEN 'view'
WHEN 'i' THEN 'index'
WHEN 'S' THEN 'sequence'
WHEN 's' THEN 'special'
END as RelationType
,pg_catalog.pg_get_userbyid(c.relowner) as RelationOwner
,pg_size_pretty(pg_relation_size(n.nspname ||'.'|| c.relname)) as RelationSize
FROM pg_catalog.pg_class c
LEFT JOIN pg_catalog.pg_namespace n
ON n.oid = c.relnamespace
WHERE c.relkind IN ('r','s')
AND (n.nspname !~ '^pg_toast' and nspname like 'pg_temp%')
ORDER BY pg_relation_size(n.nspname ||'.'|| c.relname) DESC ;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Partition_tables                                    -               |
-- +----------------------------------------------------------------------------+

\qecho <a name="Partition_tables"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Partition tables</b></font><hr align="left" width="460">

\qecho <br>
\qecho <details>
\qecho <h4>Note:The catalog pg_inherits records information about table and index inheritance hierarchies. There is one entry for each direct parent-child table or index relationship in the database</h4>
SELECT
    parent.oid                        AS parent_table_oid,
    parent.relname                    AS parent_table_name,
    count(child.oid)                  AS partition_count
FROM pg_inherits
    JOIN pg_class parent            ON pg_inherits.inhparent = parent.oid
    JOIN pg_class child             ON pg_inherits.inhrelid   = child.oid 
    group by 1,2
    order by 3 desc;


\qecho <br>

SELECT
    parent.relnamespace::regnamespace AS parent_table_schema,
    parent.relowner::regrole          AS parent_table_owner,
    parent.oid                        AS parent_table_oid,
    parent.relname                    AS parent_table_name,
  --child.relnamespace::regnamespace  AS partition_schema,
    child.oid                         AS partition_oid,
    child.relname                     AS partition_name
FROM pg_inherits
    JOIN pg_class parent            ON pg_inherits.inhparent = parent.oid
    JOIN pg_class child             ON pg_inherits.inhrelid   = child.oid 
    order by 3 ,6;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - FK_without_index                                 -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="FK_without_index"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>FK without index</b></font><hr align="left" width="460">

\qecho <br>
\qecho <details>
SELECT c.conrelid::regclass AS "table",
       /* list of key column names in order */
       string_agg(a.attname, ',' ORDER BY x.n) AS columns,
       pg_catalog.pg_size_pretty(
          pg_catalog.pg_relation_size(c.conrelid)
       ) AS size,
       c.conname AS constraint,
       c.confrelid::regclass AS referenced_table
FROM pg_catalog.pg_constraint c
   /* enumerated key column numbers per foreign key */
   CROSS JOIN LATERAL
      unnest(c.conkey) WITH ORDINALITY AS x(attnum, n)
   /* name for each key column */
   JOIN pg_catalog.pg_attribute a
      ON a.attnum = x.attnum
         AND a.attrelid = c.conrelid
WHERE NOT EXISTS
        /* is there a matching index for the constraint? */
        (SELECT 1 FROM pg_catalog.pg_index i
         WHERE i.indrelid = c.conrelid
           /* the first index columns must be the same as the
              key columns, but order doesn't matter */
           AND (i.indkey::smallint[])[0:cardinality(c.conkey)-1]
               OPERATOR(pg_catalog.@>) c.conkey)
  AND c.contype = 'f'
GROUP BY c.conrelid, c.conname, c.confrelid
ORDER BY pg_catalog.pg_relation_size(c.conrelid) DESC;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      -Table_Access_Profile                              -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Table_Access_Profile"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Table Access Profile Top 20</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details> 
with table_size_info as 
(  SELECT
     schemaname as schema_name,relname as "Table",
     pg_relation_size(relid) relation_size,
     relid,
     pg_size_pretty(pg_relation_size(relid)) AS "table_size",
     pg_size_pretty(pg_total_relation_size(relid)) AS "TABLE size + indexes",
     pg_size_pretty(pg_total_relation_size(relid) - pg_relation_size(relid)) as "indexes size"
  FROM pg_catalog.pg_statio_all_tables ORDER BY 1,3  desc
)
Select
b.schema_name,
a.relname as "Table_Name",
b.table_size as "Table_Size",
a.seq_scan  total_fts_scan ,
a.seq_tup_read total_fts_num_rows_reads,
a.seq_tup_read/NULLIF(a.seq_scan,0)  fts_rows_per_read ,
a.idx_scan total_idx_scan,
a.idx_tup_fetch total_Idx_num_rows_read ,
a.idx_tup_fetch/NULLIF(a.idx_scan,0)  idx_rows_per_read,
trunc((idx_scan::numeric/NULLIF((idx_scan::numeric+seq_scan::numeric),0)) * 100,2) as "IDX_scan_%",
trunc((seq_scan::numeric/NULLIF((idx_scan::numeric+seq_scan::numeric),0)) * 100,2) as "FTS_scan_%",
case when seq_scan>idx_scan then 'FTS' else 'IDX' end access_profile,
a.n_live_tup,
a.n_dead_tup,
trunc((n_dead_tup::numeric/NULLIF(n_live_tup::numeric,0)) * 100,2) as "dead_tup_%",
a.n_tup_ins,
a.n_tup_upd, 
a.n_tup_del,
trunc((n_tup_ins::numeric/NULLIF((n_tup_ins::numeric+n_tup_upd::numeric+n_tup_del::numeric),0)) * 100,2) as "tup_ins_%",
trunc((n_tup_upd::numeric/NULLIF((n_tup_ins::numeric+n_tup_upd::numeric+n_tup_del::numeric),0)) * 100,2) as "tup_upd_%",
trunc((n_tup_del::numeric/NULLIF((n_tup_ins::numeric+n_tup_upd::numeric+n_tup_del::numeric),0)) * 100,2) as "tup_del_%" 
from pg_stat_all_tables  a ,  table_size_info  b
where a.relid=b.relid 
and schema_name not in ('pg_catalog')
order  by b.relation_size  desc LIMIT 20;
\qecho </details>
\qecho <br>

\qecho <h3> Tables have more full table scan than index scan : </h3>
\qecho <br>
\qecho <details>
with table_size_info as 
(SELECT
schemaname as schema_name,relname as "Table",
pg_relation_size(relid) relation_size,
relid,
pg_size_pretty(pg_relation_size(relid)) AS "table_size",
pg_size_pretty(pg_total_relation_size(relid)) AS "TABLE size + indexes",
pg_size_pretty(pg_total_relation_size(relid) - pg_relation_size(relid)) as "indexes size"
FROM pg_catalog.pg_statio_all_tables ORDER BY 1,3  desc)
Select
b.schema_name,
a.relname as "Table_Name",
b.table_size as "Table_Size",
a.seq_scan  total_fts_scan ,
a.seq_tup_read total_fts_num_rows_reads,
a.seq_tup_read/NULLIF(a.seq_scan,0)  fts_rows_per_read ,
a.idx_scan total_idx_scan,
a.idx_tup_fetch total_Idx_num_rows_read ,
a.idx_tup_fetch/NULLIF(a.idx_scan,0)  idx_rows_per_read,
trunc((idx_scan::numeric/NULLIF((idx_scan::numeric+seq_scan::numeric),0)) * 100,2) as "IDX_scan_%",
trunc((seq_scan::numeric/NULLIF((idx_scan::numeric+seq_scan::numeric),0)) * 100,2) as "FTS_scan_%",
case when seq_scan>idx_scan then 'FTS' else 'IDX' end access_profile,
a.n_live_tup,
a.n_dead_tup,
trunc((n_dead_tup::numeric/NULLIF(n_live_tup::numeric,0)) * 100,2) as "dead_tup_%",
a.n_tup_ins,
a.n_tup_upd, 
a.n_tup_del,
trunc((n_tup_ins::numeric/NULLIF((n_tup_ins::numeric+n_tup_upd::numeric+n_tup_del::numeric),0)) * 100,2) as "tup_ins_%",
trunc((n_tup_upd::numeric/NULLIF((n_tup_ins::numeric+n_tup_upd::numeric+n_tup_del::numeric),0)) * 100,2) as "tup_upd_%",
trunc((n_tup_del::numeric/NULLIF((n_tup_ins::numeric+n_tup_upd::numeric+n_tup_del::numeric),0)) * 100,2) as "tup_del_%" 
from pg_stat_all_tables  a ,  table_size_info  b
where a.relid=b.relid 
and schema_name not in ('pg_catalog', 'pg_toast')
and seq_scan>idx_scan
and b.relation_size > 10485760
order by b.relation_size desc LIMIT 20;
\qecho </details>
\qecho <br>
\qecho <h4> pg_statio_all_tables View : </h4>
\qecho <h4> Total physical reads (disk blocks read or Reads from Disk) = heap_blks_read + idx_blks_read + toast_blks_read + tidx_blks_read  </h4>
\qecho <h4> Total logical reads (buffer hits or Read from Memory)  = heap_blks_hits + idx_blks_hits + toast_blks_hits + tidx_blks_hits  </h4>
\qecho <br>
\qecho <h3> Top 50 Tables by total physical reads : </h3>
\qecho <br> 
\qecho <details>
select
s2.* , 
coalesce(trunc((s2.total_physical_reads::numeric/NULLIF((s2.total_physical_reads::numeric+s2.total_logical_reads::numeric),0)) * 100,2),0)  as physical_reads_percent,
coalesce(trunc((s2.total_logical_reads::numeric/NULLIF((s2.total_physical_reads::numeric+s2.total_logical_reads::numeric),0)) * 100,2),0)  as logical_reads_percent
from 
(
select 
s.* ,
s.table_disk_blocks_read+
s.indexes_disk_blocks_read+
s.TOAST_table_disk_blocks_read+
s.TOAST_indexes_disk_blocks_read as total_physical_reads,

s.table_buffer_hits+
s.indexes_buffer_hits+
s.TOAST_table_buffer_hits+
s.TOAST_indexes_buffer_hits as total_logical_reads 
from
(
select
schemaname as schema_name,
relname as table_name,
coalesce(heap_blks_read,0) table_disk_blocks_read ,
coalesce(heap_blks_hit,0)  table_buffer_hits ,
coalesce(idx_blks_read,0) indexes_disk_blocks_read ,
coalesce(idx_blks_hit,0)   indexes_buffer_hits ,
coalesce(toast_blks_read,0) TOAST_table_disk_blocks_read ,
coalesce(toast_blks_hit,0)  TOAST_table_buffer_hits ,
coalesce(tidx_blks_read,0)  TOAST_indexes_disk_blocks_read ,
coalesce(tidx_blks_hit,0)   TOAST_indexes_buffer_hits 
from pg_statio_all_tables 
where schemaname not in ('pg_toast','pg_catalog','information_schema')
 ) as s

) as s2
order by s2.total_physical_reads  desc limit 50 ;
\qecho </details>
\qecho <br> 
\qecho <h3> Top 50 Tables by total physical reads percent  : </h3>
\qecho <br> 
\qecho <details>
select 
s2.* , 
coalesce(trunc((s2.total_physical_reads::numeric/NULLIF((s2.total_physical_reads::numeric+s2.total_logical_reads::numeric),0)) * 100,2),0)  as physical_reads_percent,
coalesce(trunc((s2.total_logical_reads::numeric/NULLIF((s2.total_physical_reads::numeric+s2.total_logical_reads::numeric),0)) * 100,2),0)  as logical_reads_percent
from 
(
select 
s.* ,
s.table_disk_blocks_read+
s.indexes_disk_blocks_read+
s.TOAST_table_disk_blocks_read+
s.TOAST_indexes_disk_blocks_read as total_physical_reads,

s.table_buffer_hits+
s.indexes_buffer_hits+
s.TOAST_table_buffer_hits+
s.TOAST_indexes_buffer_hits as total_logical_reads 
from
(
select
schemaname as schema_name,
relname as table_name,
coalesce(heap_blks_read,0) table_disk_blocks_read ,
coalesce(heap_blks_hit,0)  table_buffer_hits ,
coalesce(idx_blks_read,0) indexes_disk_blocks_read ,
coalesce(idx_blks_hit,0)   indexes_buffer_hits ,
coalesce(toast_blks_read,0) TOAST_table_disk_blocks_read ,
coalesce(toast_blks_hit,0)  TOAST_table_buffer_hits ,
coalesce(tidx_blks_read,0)  TOAST_indexes_disk_blocks_read ,
coalesce(tidx_blks_hit,0)   TOAST_indexes_buffer_hits 
from pg_statio_all_tables
where schemaname not in ('pg_toast','pg_catalog','information_schema')  
) as s

) as s2 
order by physical_reads_percent  desc limit 50  ;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - TablesWithNoPK                                   -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="TablesWithNoPK"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Tables with No PK</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho Refer to <a href="https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Source.PostgreSQL.html#CHAP_Source-PostgreSQL-DataTypes">the default mapping to AWS DMS data types from PostgreSQL source</a> and <a href="https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Target.PostgreSQL.html#CHAP_Target.PostgreSQL.DataTypes">the target PostgreSQL data types mapped from the AWS DMS data types</a>.<br>

-- START SQL
select tbl.table_schema, 
       tbl.table_name
from information_schema.tables tbl
where table_type = 'BASE TABLE'
  and table_schema not in ('pg_catalog', 'information_schema')
  and not exists (select 1 
                  from information_schema.key_column_usage kcu
                  where kcu.table_name = tbl.table_name 
                    and kcu.table_schema = tbl.table_schema);
-- END SQL
  
\qecho <br>
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>




-- +----------------------------------------------------------------------------+
-- |      - Long Running Sql                                 -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="LongRunningSql"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Long running sql (more than 10 minutes)</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>

-- START SQL
SELECT
    pid as "process id",
    usename as username,
    xact_start as "transaction start",
    current_timestamp - query_start AS runtime,
    wait_event_type,
    state,
    backend_xid,
    backend_xmin,
--\if :is_ge_10
    backend_type,
--\endif
    query
FROM pg_stat_activity
WHERE state = 'active'
	AND (current_timestamp - query_start) > interval '10 minutes'
	AND query not like '%vacuum%'
ORDER BY 1 DESC
LIMIT 10;
-- END SQL
  
\qecho <br>
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>




-- +----------------------------------------------------------------------------+
-- +----------------------------------------------------------------------------+
-- |      3 Vacuum/Auto Vacuum info                          -                  |
-- +----------------------------------------------------------------------------+
-- +----------------------------------------------------------------------------+


-- +----------------------------------------------------------------------------+
-- |      - Transaction ID TXID                                   -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Transaction_ID_TXID"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Transaction ID TXID (Wraparound)</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>

\qecho <h3>oldest xid per database:</h3>

SELECT datname database_name ,age(datfrozenxid) oldest_xid_per_DB 
FROM pg_database order by 2 limit 20;


\qecho <h3>percent_towards_emergency_autovac & percent_towards_wraparound :</h3>

WITH max_age AS ( SELECT 2000000000 as max_old_xid , setting AS 
autovacuum_freeze_max_age FROM pg_catalog.pg_settings 
WHERE name = 'autovacuum_freeze_max_age' ) , 
per_database_stats AS ( SELECT datname , m.max_old_xid::int , 
m.autovacuum_freeze_max_age::int , age(d.datfrozenxid) AS oldest_xid 
FROM pg_catalog.pg_database d JOIN max_age m ON (true) WHERE d.datallowconn ) 
SELECT max(oldest_xid) AS oldest_xid , 
max(ROUND(100*(oldest_xid/max_old_xid::float))) AS percent_towards_wraparound
 , max(ROUND(100*(oldest_xid/autovacuum_freeze_max_age::float))) AS percent_towards_emergency_autovac 
 FROM per_database_stats ;


\qecho <h3>current running autovacuum process:</h3>

SELECT datname,usename,state,query,
now() - pg_stat_activity.query_start AS duration, 
wait_event from pg_stat_activity where query like 'autovacuum:%' order by 4;



\qecho <h3>current running vacuum process:</h3>

SELECT datname,usename,state,query,
now() - pg_stat_activity.query_start AS duration,
 wait_event from pg_stat_activity where query like 'vacuum:%' order by 4;


\qecho <h3>vacuum progress process:</h3>

SELECT p.pid, now() - a.xact_start AS duration, coalesce(wait_event_type ||'.'|| wait_event, 'f') AS waiting, 
  CASE WHEN a.query ~ '^autovacuum.*to prevent wraparound' THEN 'wraparound' WHEN a.query ~ '^vacuum' THEN 'user' ELSE 'regular' END AS mode, 
  p.datname AS database, p.relid::regclass AS table, p.phase, a.query ,
  pg_size_pretty(p.heap_blks_total * current_setting('block_size')::int) AS table_size, 
  pg_size_pretty(pg_total_relation_size(p.relid)) AS total_size, 
  pg_size_pretty(p.heap_blks_scanned * current_setting('block_size')::int) AS scanned, 
  pg_size_pretty(p.heap_blks_vacuumed * current_setting('block_size')::int) AS vacuumed, 
  round(100.0 * p.heap_blks_scanned / p.heap_blks_total, 1) AS scanned_pct, 
  round(100.0 * p.heap_blks_vacuumed / p.heap_blks_total, 1) AS vacuumed_pct, 
  p.index_vacuum_count,
  p.max_dead_tuples as max_dead_tuples_per_cycle,
  s.n_dead_tup as total_num_dead_tuples ,
  ceil(s.n_dead_tup::float/p.max_dead_tuples::float) index_cycles_required
FROM pg_stat_progress_vacuum p JOIN pg_stat_activity a using (pid) 
     join pg_stat_all_tables s on s.relid = p.relid
ORDER BY now() - a.xact_start DESC;



\qecho <h3>Inactive replication slots order by age_xmin:</h3>

select *,age(xmin) age_xmin,age(catalog_xmin) age_catalog_xmin 
from pg_replication_slots where active = false order by age(xmin) desc;


\qecho <h3>active replication slots order by age_xmin:</h3>

select *,age(xmin) age_xmin,age(catalog_xmin) age_catalog_xmin 
from pg_replication_slots 
where active = true 
order by age(xmin) desc;


\qecho <h3>Orphaned prepared transactions:</h3>

SELECT gid, prepared, owner, database, age(transaction) AS ag_xmin 
FROM pg_prepared_xacts
ORDER BY age(transaction) DESC;

\qecho <h3>MAX XID held:</h3>
SELECT
(SELECT max(age(backend_xmin)) FROM pg_stat_activity) as oldest_running_xact,
(SELECT max(age(transaction)) FROM pg_prepared_xacts) as oldest_prepared_xact,
(SELECT max(age(xmin)) FROM pg_replication_slots) as oldest_replication_slot,
(SELECT max(age(backend_xmin))FROM pg_stat_replication)as oldest_replica_xact;


--\qecho <h3>XID Rate:</h3>


--SELECT max(age(datfrozenxid)) as xid1 FROM pg_database \gset 
--select pg_sleep(60);
--SELECT max(age(datfrozenxid)) as xid2 FROM pg_database \gset 
--select txid_current() current_txid \gset
     

--select (select :xid2 - :xid1)as XID_Rate,(2000000000-:current_txid) as Remaining_XIDs,
--(2000000000-:current_txid)/ ( select :xid2 - :xid1 ) /10/3600 hours_before_wraparound_prevention,
--(2000000000-:current_txid)/ ( select :xid2 - :xid1 ) /10/3600/24 days_before_wraparound_prevention
--;


\qecho <h3>Which tables are currently eligible for autovacuum ? </h3>

WITH vbt AS (SELECT setting AS autovacuum_vacuum_threshold FROM pg_settings WHERE name = 'autovacuum_vacuum_threshold')
    , vsf AS (SELECT setting AS autovacuum_vacuum_scale_factor FROM pg_settings WHERE name = 'autovacuum_vacuum_scale_factor')
    , fma AS (SELECT setting AS autovacuum_freeze_max_age FROM pg_settings WHERE name = 'autovacuum_freeze_max_age')
    , sto AS (select opt_oid, split_part(setting, '=', 1) as param, split_part(setting, '=', 2) as value from (select oid opt_oid, unnest(reloptions) setting from pg_class) opt)
SELECT
    '"'||ns.nspname||'"."'||c.relname||'"' as relation
    , pg_size_pretty(pg_table_size(c.oid)) as table_size
    , age(relfrozenxid) as xid_age
    , coalesce(cfma.value::float, autovacuum_freeze_max_age::float) autovacuum_freeze_max_age
    , (coalesce(cvbt.value::float, autovacuum_vacuum_threshold::float) + coalesce(cvsf.value::float,autovacuum_vacuum_scale_factor::float) * c.reltuples) as autovacuum_vacuum_tuples
    , n_dead_tup as dead_tuples
FROM pg_class c join pg_namespace ns on ns.oid = c.relnamespace
join pg_stat_all_tables stat on stat.relid = c.oid
join vbt on (1=1) join vsf on (1=1) join fma on (1=1)
left join sto cvbt on cvbt.param = 'autovacuum_vacuum_threshold' and c.oid = cvbt.opt_oid
left join sto cvsf on cvsf.param = 'autovacuum_vacuum_scale_factor' and c.oid = cvsf.opt_oid
left join sto cfma on cfma.param = 'autovacuum_freeze_max_age' and c.oid = cfma.opt_oid
WHERE c.relkind = 'r' and nspname <> 'pg_catalog'
and (
    age(relfrozenxid) >= coalesce(cfma.value::float, autovacuum_freeze_max_age::float)
    or
    coalesce(cvbt.value::float, autovacuum_vacuum_threshold::float) + coalesce(cvsf.value::float,autovacuum_vacuum_scale_factor::float) * c.reltuples <= n_dead_tup
   -- or 1 = 1
)
ORDER BY age(relfrozenxid) DESC ;


\qecho <h3>Total Autovacuum progress per day:</h3>

select to_char(last_autovacuum, 'YYYY-MM-DD') as date, 
count(*) from pg_stat_all_tables   
group by to_char(last_autovacuum, 'YYYY-MM-DD') order by 1;

 
\qecho <h3>When the last autovacuum succeeded?</h3> 

select relname as table_name,n_live_tup, n_tup_upd, n_tup_del, n_dead_tup, 
last_vacuum, last_autovacuum, last_analyze, last_autoanalyze 
from pg_stat_all_tables 
order by last_autovacuum desc limit 20 ;




\qecho <h3>Top-20 tables order by xid age:</h3>

-- this need to be run in each DB in the instance 

SELECT c.oid::regclass as relation_name,     
        greatest(age(c.relfrozenxid),age(t.relfrozenxid)) as age,
        pg_size_pretty(pg_table_size(c.oid)) as table_size,
        c.relkind
FROM pg_class c
LEFT JOIN pg_class t ON c.reltoastrelid = t.oid
WHERE c.relkind in ('r', 't','m')
order by 2 desc limit 20;



\qecho <h3>Indexes information for Top-20 tables order by xid age:</h3>

SELECT schemaname,relname AS tablename,
indexrelname AS indexname,
idx_scan ,
pg_relation_size(indexrelid) as index_size,
pg_size_pretty(pg_relation_size(indexrelid)) AS pretty_index_size
FROM pg_catalog.pg_stat_all_indexes
WHERE  relname in (select relation_name::text from (SELECT c.oid::regclass as relation_name,     
        greatest(age(c.relfrozenxid),age(t.relfrozenxid)) as age,
        pg_size_pretty(pg_table_size(c.oid)) as table_size,
        c.relkind
FROM pg_class c
LEFT JOIN pg_class t ON c.reltoastrelid = t.oid
WHERE c.relkind in ('r', 't','m')
order by 2 desc limit 20) as r1 )
order by 2,4 ;

\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>

-- +----------------------------------------------------------------------------+
-- |      - vacuum and Statistics           -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="vacuum_Statistics"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Vacuum & Statistics</b></font><hr align="left" width="460">
\qecho <br>
\qecho </details>
-- current running  vacuum porecess
\qecho <br>
\qecho <h3>Current running autovacuum porecess:</h3>
\qecho <br>
\qecho <details>
SELECT datname,usename,state,query,now() - pg_stat_activity.query_start AS duration, wait_event from pg_stat_activity where query like 'autovacuum:%' order by 4;
\qecho </details>
\qecho <br>
--  Whenever VACUUM is running, the pg_stat_progress_vacuum view will contain one row for each backend (including autovacuum worker processes) that is currently vacuuming (vacuum progress)
\qecho <h3>Vacuum progress:</h3>
\qecho <br>
\qecho <details>
SELECT p.pid, now() - a.xact_start AS duration, coalesce(wait_event_type ||'.'|| wait_event, 'f') AS waiting, CASE WHEN a.query ~ '^autovacuum.*to prevent wraparound' THEN 'wraparound' WHEN a.query ~ '^vacuum' THEN 'user' ELSE 'regular' END AS mode, p.datname AS database, p.relid::regclass AS table, p.phase, pg_size_pretty(p.heap_blks_total * current_setting('block_size')::int) AS table_size, pg_size_pretty(pg_total_relation_size(relid)) AS total_size, pg_size_pretty(p.heap_blks_scanned * current_setting('block_size')::int) AS scanned, pg_size_pretty(p.heap_blks_vacuumed * current_setting('block_size')::int) AS vacuumed, round(100.0 * p.heap_blks_scanned / p.heap_blks_total, 1) AS scanned_pct, round(100.0 * p.heap_blks_vacuumed / p.heap_blks_total, 1) AS vacuumed_pct, p.index_vacuum_count, round(100.0 * p.num_dead_tuples / p.max_dead_tuples,1) AS dead_pct FROM pg_stat_progress_vacuum p JOIN pg_stat_activity a using (pid) ORDER BY now() - a.xact_start DESC;
\qecho </details>
\qecho <br>
\qecho <h3>Autovacuum progress per day: </h3>
\qecho <br>
\qecho <details>
select to_char(last_autovacuum, 'YYYY-MM-DD') , count(*) from pg_stat_all_tables   group by to_char(last_autovacuum, 'YYYY-MM-DD') order by 1;
\qecho </details>
\qecho <br>
\qecho <h3>Autoanalyze progress per day: </h3>
\qecho <br>
\qecho <details>
select to_char(last_autoanalyze, 'YYYY-MM-DD') , count(*) from pg_stat_all_tables   group by to_char(last_autoanalyze, 'YYYY-MM-DD') order by 1;
\qecho </details>
\qecho <br>
--Which tables are currently eligible for autovacuum based on curret parameters
\qecho <h3>Which tables are currently eligible for autovacuum based on current parameters : </h3>
\qecho <br>
\qecho <details>
WITH vbt AS (SELECT setting AS autovacuum_vacuum_threshold FROM pg_settings WHERE name = 'autovacuum_vacuum_threshold')
    , vsf AS (SELECT setting AS autovacuum_vacuum_scale_factor FROM pg_settings WHERE name = 'autovacuum_vacuum_scale_factor')
    , fma AS (SELECT setting AS autovacuum_freeze_max_age FROM pg_settings WHERE name = 'autovacuum_freeze_max_age')
    , sto AS (select opt_oid, split_part(setting, '=', 1) as param, split_part(setting, '=', 2) as value from (select oid opt_oid, unnest(reloptions) setting from pg_class) opt)
SELECT
    '"'||ns.nspname||'"."'||c.relname||'"' as relation
    , pg_size_pretty(pg_table_size(c.oid)) as table_size
    , age(relfrozenxid) as xid_age
    , coalesce(cfma.value::float, autovacuum_freeze_max_age::float) autovacuum_freeze_max_age
    , (coalesce(cvbt.value::float, autovacuum_vacuum_threshold::float) + coalesce(cvsf.value::float,autovacuum_vacuum_scale_factor::float) * c.reltuples) as autovacuum_vacuum_tuples
    , n_dead_tup as dead_tuples
FROM pg_class c join pg_namespace ns on ns.oid = c.relnamespace
join pg_stat_all_tables stat on stat.relid = c.oid
join vbt on (1=1) join vsf on (1=1) join fma on (1=1)
left join sto cvbt on cvbt.param = 'autovacuum_vacuum_threshold' and c.oid = cvbt.opt_oid
left join sto cvsf on cvsf.param = 'autovacuum_vacuum_scale_factor' and c.oid = cvsf.opt_oid
left join sto cfma on cfma.param = 'autovacuum_freeze_max_age' and c.oid = cfma.opt_oid
WHERE c.relkind = 'r' and nspname <> 'pg_catalog'
and (
    age(relfrozenxid) >= coalesce(cfma.value::float, autovacuum_freeze_max_age::float)
    or
    coalesce(cvbt.value::float, autovacuum_vacuum_threshold::float) + coalesce(cvsf.value::float,autovacuum_vacuum_scale_factor::float) * c.reltuples <= n_dead_tup
   -- or 1 = 1
)
ORDER BY age(relfrozenxid) DESC LIMIT 50;
\qecho </details>
\qecho <br>
-- check if the statistics collector is enabled (track_counts is on)
\qecho <h3>Check if the statistics collector is enabled (track_counts is on) : </h3>
\qecho <br>
\qecho <details>
SELECT name, setting FROM pg_settings WHERE name='track_counts';
\qecho </details>
\qecho <br>
-- to check the number of dead rows for the top 50 table
\qecho <h3>Number of dead rows for the top 50 table : </h3>
\qecho <br>
\qecho <details>
select relname,n_live_tup, n_tup_upd, n_tup_del, n_dead_tup, last_vacuum, last_autovacuum, last_analyze, last_autoanalyze  from pg_stat_all_tables order by n_dead_tup desc limit 50;
\qecho </details>
\qecho <br>
\qecho <h3>Tables have more than 10% dead rows :</h3>
\qecho <br>
\qecho <details>
select schemaname,relname , last_vacuum,last_autovacuum,n_live_tup,n_dead_tup , trunc((n_dead_tup::numeric/nullif(n_live_tup+n_dead_tup,0))* 100,2) as "n_dead_tup_%" from pg_stat_user_tables where n_dead_tup::float/nullif(n_live_tup+n_dead_tup,0) >.1 order by n_live_tup desc ;
\qecho </details>
\qecho <br>
\qecho <h3>pg_stat_all_tables : </h3>
\qecho <br>
\qecho <details>
select relname,schemaname,last_vacuum,last_autovacuum,last_analyze,last_autoanalyze,vacuum_count,autovacuum_count,analyze_count,autoanalyze_count from pg_stat_all_tables  where schemaname not in ('pg_catalog','pg_toast') order by 2;
\qecho </details>
\qecho <br>
\qecho <h3>Tables without auto analyze : </h3>
\qecho <br>
\qecho <details>
select count(*) from pg_stat_all_tables  where  autoanalyze_count = 0 ;
\qecho <br>
select relname,schemaname,last_vacuum,last_autovacuum,autovacuum_count,autoanalyze_count,last_analyze,last_autoanalyze,n_mod_since_analyze from pg_stat_all_tables  where  autoanalyze_count = 0  order by 2;
\qecho </details>
\qecho <br>
\qecho <h3>tables without auto vacuum : </h3>
\qecho <br>
\qecho <details>
select count(*) from pg_stat_all_tables  where autovacuum_count  = 0 ;
\qecho <br>
select relname,schemaname,last_vacuum,last_autovacuum,autovacuum_count,autoanalyze_count,last_analyze,last_autoanalyze,n_dead_tup from pg_stat_all_tables  where autovacuum_count  = 0  order by 2;
\qecho </details>
\qecho <br>
\qecho <h3>Tables that have not been manually analyzed :</h3>
\qecho <br>
\qecho <details>
select count (analyze_count) from pg_stat_all_tables where analyze_count = 0;
\qecho <br>
select relname,schemaname,last_vacuum,vacuum_count,last_autovacuum,autovacuum_count,last_autoanalyze,autoanalyze_count,last_analyze,analyze_count from pg_stat_all_tables  where analyze_count = 0;
\qecho </details>
\qecho <br>
\qecho <h3>tables without auto analyze,auto vacuum,vacuum and analyze : </h3>
\qecho <br>
\qecho <details>
select count (*) from pg_stat_all_tables  where  autoanalyze_count = 0 and autovacuum_count  = 0 and analyze_count = 0 and vacuum_count=0 ;
\qecho <br>
select relname,schemaname,last_vacuum,vacuum_count,last_autovacuum,autovacuum_count,last_autoanalyze,autoanalyze_count,last_analyze,analyze_count from pg_stat_all_tables  where  autoanalyze_count = 0 and autovacuum_count  = 0 and analyze_count = 0 and vacuum_count=0 ;  
\qecho </details>
\qecho <br>
-- to show tables that have specific table-level parameters set
\qecho <h3>Tables that have specific table-level parameters set : </h3>
\qecho <br>
\qecho <details>
select relname, reloptions from pg_class where reloptions is not null;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>


-- +----------------------------------------------------------------------------+
-- |      - Fragmentation                                    -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Fragmentation"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Fragmentation (Bloat)</b></font><hr align="left" width="460">
-- Show database bloat
\qecho <br>
\qecho <h3>Top 50 Tables Bloats order by "bloat%" having "bolat%">0 :</h3>
\qecho <br>
\qecho <details>
/* WARNING: executed with a non-superuser role, the query inspect only tables and materialized view (9.3+) you are granted to read.
* This query is compatible with PostgreSQL 9.0 and more
*/
-- This query is taken from https://github.com/ioguix/pgsql-bloat-estimation/blob/master/table/table_bloat.sql
SELECT current_database(), schemaname, tblname, bs*tblpages AS real_size,
  (tblpages-est_tblpages)*bs AS extra_size,
  CASE WHEN tblpages > 0 AND tblpages - est_tblpages > 0
    THEN 100 * (tblpages - est_tblpages)/tblpages::float
    ELSE 0
  END AS extra_pct, fillfactor,
  CASE WHEN tblpages - est_tblpages_ff > 0
    THEN (tblpages-est_tblpages_ff)*bs
    ELSE 0
  END AS bloat_size,
  CASE WHEN tblpages > 0 AND tblpages - est_tblpages_ff > 0
    THEN 100 * (tblpages - est_tblpages_ff)/tblpages::float
    ELSE 0
  END AS bloat_pct, is_na
  -- , tpl_hdr_size, tpl_data_size, (pst).free_percent + (pst).dead_tuple_percent AS real_frag -- (DEBUG INFO)
FROM (
  SELECT ceil( reltuples / ( (bs-page_hdr)/tpl_size ) ) + ceil( toasttuples / 4 ) AS est_tblpages,
    ceil( reltuples / ( (bs-page_hdr)*fillfactor/(tpl_size*100) ) ) + ceil( toasttuples / 4 ) AS est_tblpages_ff,
    tblpages, fillfactor, bs, tblid, schemaname, tblname, heappages, toastpages, is_na
    -- , tpl_hdr_size, tpl_data_size, pgstattuple(tblid) AS pst -- (DEBUG INFO)
  FROM (
    SELECT
      ( 4 + tpl_hdr_size + tpl_data_size + (2*ma)
        - CASE WHEN tpl_hdr_size%ma = 0 THEN ma ELSE tpl_hdr_size%ma END
        - CASE WHEN ceil(tpl_data_size)::int%ma = 0 THEN ma ELSE ceil(tpl_data_size)::int%ma END
      ) AS tpl_size, bs - page_hdr AS size_per_block, (heappages + toastpages) AS tblpages, heappages,
      toastpages, reltuples, toasttuples, bs, page_hdr, tblid, schemaname, tblname, fillfactor, is_na
      -- , tpl_hdr_size, tpl_data_size
    FROM (
      SELECT
        tbl.oid AS tblid, ns.nspname AS schemaname, tbl.relname AS tblname, tbl.reltuples,
        tbl.relpages AS heappages, coalesce(toast.relpages, 0) AS toastpages,
        coalesce(toast.reltuples, 0) AS toasttuples,
        coalesce(substring(
          array_to_string(tbl.reloptions, ' ')
          FROM 'fillfactor=([0-9]+)')::smallint, 100) AS fillfactor,
        current_setting('block_size')::numeric AS bs,
        CASE WHEN version()~'mingw32' OR version()~'64-bit|x86_64|ppc64|ia64|amd64' THEN 8 ELSE 4 END AS ma,
        24 AS page_hdr,
        23 + CASE WHEN MAX(coalesce(s.null_frac,0)) > 0 THEN ( 7 + count(s.attname) ) / 8 ELSE 0::int END
           + CASE WHEN bool_or(att.attname = 'oid' and att.attnum < 0) THEN 4 ELSE 0 END AS tpl_hdr_size,
        sum( (1-coalesce(s.null_frac, 0)) * coalesce(s.avg_width, 0) ) AS tpl_data_size,
        bool_or(att.atttypid = 'pg_catalog.name'::regtype)
          OR sum(CASE WHEN att.attnum > 0 THEN 1 ELSE 0 END) <> count(s.attname) AS is_na
      FROM pg_attribute AS att
        JOIN pg_class AS tbl ON att.attrelid = tbl.oid
        JOIN pg_namespace AS ns ON ns.oid = tbl.relnamespace
        LEFT JOIN pg_stats AS s ON s.schemaname=ns.nspname
          AND s.tablename = tbl.relname AND s.inherited=false AND s.attname=att.attname
        LEFT JOIN pg_class AS toast ON tbl.reltoastrelid = toast.oid
      WHERE NOT att.attisdropped
        AND tbl.relkind in ('r','m')
      GROUP BY 1,2,3,4,5,6,7,8,9,10
      ORDER BY 2,3
    ) AS s
  ) AS s2
) AS s3
-- WHERE NOT is_na
--   AND tblpages*((pst).free_percent + (pst).dead_tuple_percent)::float4/100 >= 1
WHERE CASE WHEN tblpages > 0 AND tblpages - est_tblpages_ff > 0
    THEN 100 * (tblpages - est_tblpages_ff)/tblpages::float
    ELSE 0
  END > 0
  AND schemaname not in('information_schema')
ORDER BY bloat_pct desc, real_size desc, schemaname, tblname
LIMIT 50;
\qecho </details>

\qecho <br>
\qecho <h3>Top 50 Indexes Bloats order by "bloat%" having "bolat%">0 :</h3>
\qecho <br>
\qecho <details>
-- WARNING: executed with a non-superuser role, the query inspect only index on tables you are granted to read.
-- WARNING: rows with is_na = 't' are known to have bad statistics ("name" type is not supported).
-- This query is compatible with PostgreSQL 8.2 and after
-- This query is taken from https://github.com/ioguix/pgsql-bloat-estimation/blob/master/btree/btree_bloat.sql
SELECT current_database(), nspname AS schemaname, tblname, idxname, bs*(relpages)::bigint AS real_size,
  bs*(relpages-est_pages)::bigint AS extra_size,
  100 * (relpages-est_pages)::float / relpages AS extra_pct,
  fillfactor,
  CASE WHEN relpages > est_pages_ff
    THEN bs*(relpages-est_pages_ff)
    ELSE 0
  END AS bloat_size,
  100 * (relpages-est_pages_ff)::float / relpages AS bloat_pct,
  is_na
  -- , 100-(pst).avg_leaf_density AS pst_avg_bloat, est_pages, index_tuple_hdr_bm, maxalign, pagehdr, nulldatawidth, nulldatahdrwidth, reltuples, relpages -- (DEBUG INFO)
FROM (
  SELECT coalesce(1 +
         ceil(reltuples/floor((bs-pageopqdata-pagehdr)/(4+nulldatahdrwidth)::float)), 0 -- ItemIdData size + computed avg size of a tuple (nulldatahdrwidth)
      ) AS est_pages,
      coalesce(1 +
         ceil(reltuples/floor((bs-pageopqdata-pagehdr)*fillfactor/(100*(4+nulldatahdrwidth)::float))), 0
      ) AS est_pages_ff,
      bs, nspname, tblname, idxname, relpages, fillfactor, is_na
      -- , pgstatindex(idxoid) AS pst, index_tuple_hdr_bm, maxalign, pagehdr, nulldatawidth, nulldatahdrwidth, reltuples -- (DEBUG INFO)
  FROM (
      SELECT maxalign, bs, nspname, tblname, idxname, reltuples, relpages, idxoid, fillfactor,
            ( index_tuple_hdr_bm +
                maxalign - CASE -- Add padding to the index tuple header to align on MAXALIGN
                  WHEN index_tuple_hdr_bm%maxalign = 0 THEN maxalign
                  ELSE index_tuple_hdr_bm%maxalign
                END
              + nulldatawidth + maxalign - CASE -- Add padding to the data to align on MAXALIGN
                  WHEN nulldatawidth = 0 THEN 0
                  WHEN nulldatawidth::integer%maxalign = 0 THEN maxalign
                  ELSE nulldatawidth::integer%maxalign
                END
            )::numeric AS nulldatahdrwidth, pagehdr, pageopqdata, is_na
            -- , index_tuple_hdr_bm, nulldatawidth -- (DEBUG INFO)
      FROM (
          SELECT n.nspname, i.tblname, i.idxname, i.reltuples, i.relpages,
              i.idxoid, i.fillfactor, current_setting('block_size')::numeric AS bs,
              CASE -- MAXALIGN: 4 on 32bits, 8 on 64bits (and mingw32 ?)
                WHEN version() ~ 'mingw32' OR version() ~ '64-bit|x86_64|ppc64|ia64|amd64' THEN 8
                ELSE 4
              END AS maxalign,
              /* per page header, fixed size: 20 for 7.X, 24 for others */
              24 AS pagehdr,
              /* per page btree opaque data */
              16 AS pageopqdata,
              /* per tuple header: add IndexAttributeBitMapData if some cols are null-able */
              CASE WHEN max(coalesce(s.null_frac,0)) = 0
                  THEN 8 -- IndexTupleData size
                  ELSE 8 + (( 32 + 8 - 1 ) / 8) -- IndexTupleData size + IndexAttributeBitMapData size ( max num filed per index + 8 - 1 /8)
              END AS index_tuple_hdr_bm,
              /* data len: we remove null values save space using it fractionnal part from stats */
              sum( (1-coalesce(s.null_frac, 0)) * coalesce(s.avg_width, 1024)) AS nulldatawidth,
              max( CASE WHEN i.atttypid = 'pg_catalog.name'::regtype THEN 1 ELSE 0 END ) > 0 AS is_na
          FROM (
              SELECT ct.relname AS tblname, ct.relnamespace, ic.idxname, ic.attpos, ic.indkey, ic.indkey[ic.attpos], ic.reltuples, ic.relpages, ic.tbloid, ic.idxoid, ic.fillfactor,
                  coalesce(a1.attnum, a2.attnum) AS attnum, coalesce(a1.attname, a2.attname) AS attname, coalesce(a1.atttypid, a2.atttypid) AS atttypid,
                  CASE WHEN a1.attnum IS NULL
                  THEN ic.idxname
                  ELSE ct.relname
                  END AS attrelname
              FROM (
                  SELECT idxname, reltuples, relpages, tbloid, idxoid, fillfactor, indkey,
                      pg_catalog.generate_series(1,indnatts) AS attpos
                  FROM (
                      SELECT ci.relname AS idxname, ci.reltuples, ci.relpages, i.indrelid AS tbloid,
                          i.indexrelid AS idxoid,
                          coalesce(substring(
                              array_to_string(ci.reloptions, ' ')
                              from 'fillfactor=([0-9]+)')::smallint, 90) AS fillfactor,
                          i.indnatts,
                          pg_catalog.string_to_array(pg_catalog.textin(
                              pg_catalog.int2vectorout(i.indkey)),' ')::int[] AS indkey
                      FROM pg_catalog.pg_index i
                      JOIN pg_catalog.pg_class ci ON ci.oid = i.indexrelid
                      WHERE ci.relam=(SELECT oid FROM pg_am WHERE amname = 'btree')
                      AND ci.relpages > 0
                  ) AS idx_data
              ) AS ic
              JOIN pg_catalog.pg_class ct ON ct.oid = ic.tbloid
              LEFT JOIN pg_catalog.pg_attribute a1 ON
                  ic.indkey[ic.attpos] <> 0
                  AND a1.attrelid = ic.tbloid
                  AND a1.attnum = ic.indkey[ic.attpos]
              LEFT JOIN pg_catalog.pg_attribute a2 ON
                  ic.indkey[ic.attpos] = 0
                  AND a2.attrelid = ic.idxoid
                  AND a2.attnum = ic.attpos
            ) i
            JOIN pg_catalog.pg_namespace n ON n.oid = i.relnamespace
            JOIN pg_catalog.pg_stats s ON s.schemaname = n.nspname
                                      AND s.tablename = i.attrelname
                                      AND s.attname = i.attname
            GROUP BY 1,2,3,4,5,6,7,8,9,10,11
      ) AS rows_data_stats
  ) AS rows_hdr_pdg_stats
) AS relation_stats
WHERE 100 * (relpages-est_pages_ff)::float / relpages > 0
  AND nspname not in('information_schema')
ORDER BY bloat_pct desc, real_size desc, nspname, tblname, idxname
LIMIT 50;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>





-- +----------------------------------------------------------------------------+
-- +----------------------------------------------------------------------------+
-- |      4 Others                                           -                  |
-- +----------------------------------------------------------------------------+
-- +----------------------------------------------------------------------------+



-- +----------------------------------------------------------------------------+
-- |      - Memory setting                                   -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Memory_setting"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Memory setting</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho cach read hit for the whole instance
select 
round((sum(blks_hit)::numeric / (sum(blks_hit) + sum(blks_read)::numeric))*100,2) as cache_read_hit_percentage
from pg_stat_database ;
\qecho <br>
\qecho cach read hit per Database 
select datname as database_name, 
round((blks_hit::numeric / (blks_hit + blks_read)::numeric)*100,2) as cache_read_hit_percentage
from pg_stat_database 
where blks_hit + blks_read > 0
and datname is not null 
order by 2 desc;
\qecho </details>
\qecho <br>
\qecho cach read hit per table
\qecho <br>
\qecho <details>
SELECT schemaname,relname as table_name,
 round((heap_blks_hit::numeric / (heap_blks_hit + heap_blks_read)::numeric)*100,2) as read_hit_percentage
FROM 
  pg_statio_all_tables
  where heap_blks_hit + heap_blks_read > 0
  and schemaname not in ('pg_catalog','information_schema')
  order by 3;
  \qecho </details>
\qecho <br>
\qecho cach read hit per index 
\qecho <br>
\qecho <details>
SELECT schemaname,relname as table_name,indexrelname as index_name ,
 round((idx_blks_hit::numeric / (idx_blks_hit + idx_blks_read)::numeric)*100,2) as read_hit_percentage
FROM 
  pg_statio_all_indexes
  where idx_blks_hit + idx_blks_read > 0
  and schemaname not in ('pg_catalog','information_schema')
  order by 4;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>


-- +----------------------------------------------------------------------------+
-- |      - pg_stat_statements extension                                    -   |
-- +----------------------------------------------------------------------------+

\qecho <a name="pg_stat_statements_extension"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>pg_stat_statements extension</b></font><hr align="left" width="460">
\qecho <br>
\if :pg_stat_stmts_is_available
  \qecho <h3> pg_stat_statements installed version: </h3>
  \qecho <br>
  \qecho <details>
  SELECT e.extname AS "Extension Name", e.extversion AS "Version", n.nspname AS "Schema",pg_get_userbyid(e.extowner)  as Owner, c.description AS "Description" , e.extrelocatable as "relocatable to another schema", e.extconfig ,e.extcondition
  FROM pg_catalog.pg_extension e LEFT JOIN pg_catalog.pg_namespace n ON n.oid = e.extnamespace LEFT JOIN pg_catalog.pg_description c ON c.objoid = e.oid AND c.classoid = 'pg_catalog.pg_extension'::pg_catalog.regclass
  where e.extname = 'pg_stat_statements';
  \qecho <br>
  \qecho <h3>Parameters values: </h3>
  -- pg_stat_statements extension configuration 
  select name as parameter_name, setting  from pg_settings where name in ('pg_stat_statements.track','pg_stat_statements.track_utility','pg_stat_statements.save'
  ,'pg_stat_statements.max','shared_preload_libraries');
  \qecho <br>
  \qecho <h3>Available versions that are available to upgrade: </h3>
  select * from
  (
  select b.name as extension_name , b.version as version ,b.installed as installed
  from
  (SELECT extname ,extversion FROM pg_extension) a ,
  (SELECT name ,version ,installed FROM pg_available_extension_versions where name in (SELECT extname FROM pg_extension)) b
  where a.extname = b.name
  and b.version > a.extversion
  order by b.name , b.version
  ) as r
  where r.extension_name='pg_stat_statements';

  \qecho <br>
  \qecho <h3>Latest Extension version that is available to upgrade: </h3>
  select * from
  (
  select name as extension_name , max(version) as latest_version
  from
  (select b.name , b.version ,b.installed
  from
  (SELECT extname ,extversion FROM pg_extension) a ,
  (SELECT name ,version ,installed FROM pg_available_extension_versions where name in (SELECT extname FROM pg_extension)) b
  where a.extname = b.name
  and b.version > a.extversion
  order by b.name , b.version ) e
  group by name
  ) as r
  where r.extension_name='pg_stat_statements';

  \qecho </details>

  \qecho <br>
  \qecho <h3> Top SQL order by total_exec_time: </h3>
  \qecho <br>
  \qecho <details>

  --Top SQL order by total_exec_time
    select queryid,substring(query,1,60) as query , calls, 
          round(total_exec_time::numeric, 2) as total_time_Msec, 
          round((total_exec_time::numeric/1000), 2) as total_time_sec,
          round(mean_exec_time::numeric,2) as avg_time_Msec,
          round((mean_exec_time::numeric/1000),2) as avg_time_sec,
          round(stddev_exec_time::numeric, 2) as standard_deviation_time_Msec, 
          round((stddev_exec_time::numeric/1000), 2) as standard_deviation_time_sec, 
          round(rows::numeric/calls,2) rows_per_exec,
          round((100 * total_exec_time / sum(total_exec_time) over ())::numeric, 4) as percent
          from pg_stat_statements 
          order by total_time_Msec desc limit 20; 



  \qecho </details>

  \qecho <br>
  \qecho <h3> Top SQL order by avg_time: </h3>
  \qecho <br>
  \qecho <details>
  --Top SQL order by avg_time
    select queryid,substring(query,1,60) as query , calls, 
          round(total_exec_time::numeric, 2) as total_time_Msec, 
          round((total_exec_time::numeric/1000), 2) as total_time_sec,
          round(mean_exec_time::numeric,2) as avg_time_Msec,
          round((mean_exec_time::numeric/1000),2) as avg_time_sec,
          round(stddev_exec_time::numeric, 2) as standard_deviation_time_Msec, 
          round((stddev_exec_time::numeric/1000), 2) as standard_deviation_time_sec, 
          round(rows::numeric/calls,2) rows_per_exec,
          round((100 * total_exec_time / sum(total_exec_time) over ())::numeric, 4) as percent
          from pg_stat_statements 
          order by avg_time_Msec desc limit 20;

  \qecho </details>
  \qecho <br>
  \qecho <h3> Top SQL order by percent of total DB time percent: </h3>
  \qecho <br>
  \qecho <details>
  --Top SQL order by percent of total DB time
    select queryid,substring(query,1,60) as query , calls, 
          round(total_exec_time::numeric, 2) as total_time_Msec, 
          round((total_exec_time::numeric/1000), 2) as total_time_sec,
          round(mean_exec_time::numeric,2) as avg_time_Msec,
          round((mean_exec_time::numeric/1000),2) as avg_time_sec,
          round(stddev_exec_time::numeric, 2) as standard_deviation_time_Msec, 
          round((stddev_exec_time::numeric/1000), 2) as standard_deviation_time_sec, 
          round(rows::numeric/calls,2) rows_per_exec,
          round((100 * total_exec_time / sum(total_exec_time) over ())::numeric, 4) as percent
          from pg_stat_statements 
          order by percent desc limit 20;
  \qecho </details>
  \qecho <br>
  \qecho <h3> Top SQL order by number of execution (CALLs): </h3>
  \qecho <br>
  \qecho <details>
  --Top SQL order by number of execution (CALLs)
    select queryid,substring(query,1,60) as query , calls, 
          round(total_exec_time::numeric, 2) as total_time_Msec, 
          round((total_exec_time::numeric/1000), 2) as total_time_sec,
          round(mean_exec_time::numeric,2) as avg_time_Msec,
          round((mean_exec_time::numeric/1000),2) as avg_time_sec,
          round(stddev_exec_time::numeric, 2) as standard_deviation_time_Msec, 
          round((stddev_exec_time::numeric/1000), 2) as standard_deviation_time_sec, 
          round(rows::numeric/calls,2) rows_per_exec,
          round((100 * total_exec_time / sum(total_exec_time) over ())::numeric, 4) as percent
          from pg_stat_statements 
          order by calls desc limit 20;
  \qecho </details>
  \qecho <br>
  \qecho <h3> Top SQL order by shared blocks read (physical reads): </h3>
  \qecho <br>
  \qecho <details>
  --Top SQL order by shared blocks read (physical reads)
    select queryid,substring(query,1,60) as query , calls, 
          round(total_exec_time::numeric, 2) as total_time_Msec, 
          round((total_exec_time::numeric/1000), 2) as total_time_sec,
          round(mean_exec_time::numeric,2) as avg_time_Msec,
          round((mean_exec_time::numeric/1000),2) as avg_time_sec,
          round(stddev_exec_time::numeric, 2) as standard_deviation_time_Msec, 
          round((stddev_exec_time::numeric/1000), 2) as standard_deviation_time_sec, 
          round(rows::numeric/calls,2) rows_per_exec,
          round((100 * total_exec_time / sum(total_exec_time) over ())::numeric, 4) as percent,
          shared_blks_read
          from pg_stat_statements 
          order by shared_blks_read desc limit 20;
  \qecho </details>

\else
  \qecho Notice: This section is not populated as pg_stat_statements is not installed.
\endif

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Replication                                   -                  |
-- +----------------------------------------------------------------------------+
\qecho <a name="Replication"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Replication</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho <h3> Active replication slots order by age_xmin:</h3> 
select *,age(xmin) age_xmin,age(catalog_xmin) age_catalog_xmin 
from pg_replication_slots 
where active = true 
order by age(xmin) desc;
\qecho <br>
\qecho <h3> Replication Slot Lag:</h3> 
select slot_name,slot_type,database,active,
coalesce(round(pg_wal_lsn_diff(pg_current_wal_lsn(), restart_lsn) / 1024 / 1024 , 2),0) AS Lag_MB_behind ,
coalesce(round(pg_wal_lsn_diff(pg_current_wal_lsn(), restart_lsn) / 1024 / 1024 / 1024, 2),0) AS Lag_GB_behind
from pg_replication_slots 
order by Lag_MB_behind desc;
\qecho <br>
\qecho <h3> Inactive replication slots order by age_xmin::</h3> 
select *,age(xmin) age_xmin,age(catalog_xmin) age_catalog_xmin 
from pg_replication_slots where active = false order by age(xmin) desc;
\qecho <br>
\qecho <h3> Note:</h3>
\qecho <p> RDS Postgres instance storage may get full because inactive replication slots were not removed after DMS task completed </p>
\qecho <p> If replication slot is created and it becomes in-active, then transaction logs wont recycle from master instance. So eventually storage gets full </p> 
\qecho <p> These replication slots can be cleaned as below </p>
\qecho <br>
\qecho <h4> Drop inactive replication slot : </h4>
\qecho <p> Use the below SQL to Generate SQL to drop the inactive slots </p>
\qecho <p>  select 'select pg_drop_replication_slot('''||slot_name||''');' from pg_replication_slots where active = false; </p>
\qecho <p> then Verify the CLoudWatch metrics Free Storage Space to confirm that disk space was released </p>
\qecho <br>
\qecho <h3>Inspecting current Replication state: </h3>
SELECT * FROM pg_stat_replication;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>




-- +----------------------------------------------------------------------------+
-- |      - Current Temporary File Usgae                  -                     |
-- +----------------------------------------------------------------------------+

\qecho <a name="CurrentTemporaryFileUsage"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Current Temporary File Usage</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho <p> "log_temp_files" parameter sends messages to the postgresql.log when the temporary files of a session are removed. </p>
\qecho <p> This means, "log_temp_files" parameter produces logs after a query successfully completes. Therefore, it might not help in troubleshooting active, long-running queries creating temp files. </p>
\qecho <br>
\if :is_ge_13
  select * from pg_ls_tmpdir();
\else
  \qecho pg_ls_tmpdir() function is not supported for PostgreSQL versions < 13
\endif
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Toast_Tables_Mapping                             -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Toast_Tables_Mapping"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Toast Tables Mapping</b></font><hr align="left" width="460">
\qecho <br>
\qecho This section provides information about toast tables having size > 0.
\qecho <details>
SELECT t.relname table_name, r.relname toast_name, pg_size_pretty(pg_relation_size(t.reltoastrelid)) as toast_size
FROM
    pg_class r
INNER JOIN pg_class t ON r.oid = t.reltoastrelid
WHERE pg_relation_size(t.reltoastrelid) > 0
ORDER BY r.relname ;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - sessions_info                                                     - |
-- +----------------------------------------------------------------------------+

\qecho <a name="sessions_info"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Sessions/Connections Info</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho <h3>Connections utilization:</h3>
with
settings as (SELECT setting::float AS "max_connections" FROM pg_settings WHERE name = 'max_connections'),
connections as (select sum (numbackends)::float total_connections from pg_stat_database)
select   settings.max_connections AS "Max_connections" ,total_connections as "Total_connections",ROUND((100*(connections.Total_connections/settings.max_connections))::numeric,2) as "Connections utilization %" from  settings, connections;
\qecho <h3> DB/Connections count :</h3>
SELECT datname as "Database_Name",count(*) as "Connections_count" FROM pg_stat_activity where datname is not null group by datname order by 2 desc;
\qecho <h3> DB/username/Connections count :</h3>
SELECT datname as "Database_Name",usename as "User_Name" ,count(*) as "Connections_count" FROM pg_stat_activity  where datname is not null  group by datname,usename order by 1,3 desc;
\qecho <h3> username/Connections count :</h3>
SELECT usename as "User_Name",count(*) as "connections_count" FROM pg_stat_activity  where datname is not null  group by usename order by 2 desc;
\qecho <h3> username/status/Connections count :</h3>
SELECT usename as "User_Name",state as status,count(*) as "Connections_count" FROM pg_stat_activity  where datname is not null group by usename,state order by 1,2 desc;
\qecho <h3> status/Connections count :</h3>
SELECT state as status ,count(*) as "Connections_count" FROM pg_stat_activity where datname is not null GROUP BY status order by 2 desc;
\qecho <h3> username/status/SQL/count : </h3>
SELECT usename as "User_Name" , state as status , query, count(*) FROM pg_stat_activity where datname is not null group by usename,state,query ;
\qecho </details>

\qecho <br>
\qecho <h3>Active sessions:</h3>
\qecho <br>
\qecho <details>
/* active_session_monitor*/ select * from
(
    SELECT
usename,pid, now() - pg_stat_activity.xact_start AS xact_duration ,now() - pg_stat_activity.query_start AS query_duration,
substr(query,1,50) as query,state,wait_event
FROM pg_stat_activity
) as s where (xact_duration is not null  or query_duration is not null ) and state!='idle' and query not like '%active_session_monitor%'
order by xact_duration desc, query_duration desc;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>


-- +----------------------------------------------------------------------------+
-- |      - Orphaned_prepared_transactions                   -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Orphaned_prepared_transactions"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Orphaned prepared transactions</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
SELECT gid, prepared, owner, database, transaction AS xmin
FROM pg_prepared_xacts
ORDER BY age(transaction) DESC; 

\qecho <br>
\qecho <h3> Note:</h3>
\qecho <p> During two-phase commit, a distributed transaction is first prepared with the PREPARE statement and then committed with the COMMIT PREPARED statement </p>
\qecho <p> Once a transaction has been prepared, it is kept hanging around until it is committed or aborted. It </p> 
\qecho <p> even has to survive a server restart! Normally, transactions don not remain in the prepared state for long,  </p>
\qecho <p> but sometimes things go wrong and a prepared transaction has to be removed manually by an administrator.  </p>
\qecho <p> any Orphaned prepared transactions will prevent the VACUUM to remove the dead rows   </p>           
\qecho <p>  EXAMPLE: DETAIL:  50000 dead row versions cannot be removed yet,oldest xmin: 22300 </p>
\qecho <br>
\qecho <p>  Use the ROLLBACK PREPARED transaction_id SQL statement to  remove prepared transactions    </p>
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>


-- +----------------------------------------------------------------------------+
-- |      - access_privileges                                    -              |
-- +----------------------------------------------------------------------------+

\qecho <a name="access_privileges"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Access privileges</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
SELECT n.nspname as "Schema",
  c.relname as "Name",
  CASE c.relkind WHEN 'r' THEN 'table' WHEN 'v' THEN 'view' WHEN 'm' THEN 'materialized view' WHEN 'S' THEN 'sequence' WHEN 'f' THEN 'foreign table' END as "Type",
  pg_catalog.array_to_string(c.relacl, E'\n') AS "Access privileges",
  pg_catalog.array_to_string(ARRAY(
    SELECT attname || E':\n  ' || pg_catalog.array_to_string(attacl, E'\n  ')
    FROM pg_catalog.pg_attribute a
    WHERE attrelid = c.oid AND NOT attisdropped AND attacl IS NOT NULL
  ), E'\n') AS "Column access privileges"
FROM pg_catalog.pg_class c
     LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
WHERE c.relkind IN ('r', 'v', 'm', 'S', 'f')
  AND n.nspname !~ '^pg_' AND pg_catalog.pg_table_is_visible(c.oid)
ORDER BY 1, 3, 2;
\qecho </details>

\qecho <br>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Default access privileges</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
SELECT pg_catalog.pg_get_userbyid(d.defaclrole) AS "Owner",
  n.nspname AS "Schema",
  CASE d.defaclobjtype WHEN 'r' THEN 'table' WHEN 'S' THEN 'sequence' WHEN 'f' THEN 'function' WHEN 'T' THEN 'type' WHEN 'n' THEN 'schema' ELSE 'unknown' END AS "Type", 
pg_catalog.array_to_string(d.defaclacl, E'\n') AS "Access privileges"
FROM pg_catalog.pg_default_acl d
     LEFT JOIN pg_catalog.pg_namespace n ON n.oid = d.defaclnamespace
ORDER BY 1, 2, 3;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - pgaudit_extension                                  -                |
-- +----------------------------------------------------------------------------+

\qecho <a name="pgaudit_extension"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>pgaudit extension</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
SELECT e.extname AS "Extension Name", e.extversion AS "Version", n.nspname AS "Schema",pg_get_userbyid(e.extowner)  as Owner, c.description AS "Description" , e.extrelocatable as "relocatable to another schema", e.extconfig ,e.extcondition
 FROM pg_catalog.pg_extension e LEFT JOIN pg_catalog.pg_namespace n ON n.oid = e.extnamespace LEFT JOIN pg_catalog.pg_description c ON c.objoid = e.oid AND c.classoid = 'pg_catalog.pg_extension'::pg_catalog.regclass
 where e.extname = 'pgaudit';
\qecho <br>
SELECT name as "parameter_name", setting from pg_settings where name like 'pgaudit.%' or name = 'shared_preload_libraries';
\qecho <br>
\qecho <h3>pgaudit user level configuration : </h3>
select usename as user_name,useconfig as user_config FROM pg_user where useconfig::text like '%pgaudit.%';
\qecho </details>


\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>


-- +----------------------------------------------------------------------------+
-- |      - ssl   -                                                             |
-- +----------------------------------------------------------------------------+

\qecho <a name="ssl"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>SSL</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
select name as "Parameter_Name" , setting as value,short_desc  from pg_settings where name like '%ssl%';
\qecho <br>
select version as ssl_version , count (*) as "Connection_count" FROM pg_stat_ssl group by version ;
\qecho <br>
select ssl , count (*) as "Connection_count" FROM pg_stat_ssl group by ssl ;
\qecho <br>
SELECT datname as "Database_Name" ,usename as "User_Name", ssl , client_addr , application_name, backend_type
FROM pg_stat_ssl
JOIN pg_stat_activity
ON pg_stat_ssl.pid = pg_stat_activity.pid
order by ssl ;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - background_processes                                   -            |
-- +----------------------------------------------------------------------------+

\qecho <a name="background_processes"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Background processes</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho <h3>Count of Postgres background processess : </h3>
select count (*) as "Background processes count" FROM pg_stat_activity where datname is null ;
\qecho <br>
SELECT  pid , backend_type as  "Background processes Type" , backend_start as "start time" FROM pg_stat_activity where datname is null order by 3 ;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>




-- +----------------------------------------------------------------------------+
-- |      - Multixact ID MXID                                -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Multixact_ID_MXID"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Multixact ID MXID (Wraparound)</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho <h3>oldest mxid::</h3>
SELECT max(mxid_age(datminmxid)) oldest_mxid FROM pg_database ;

\qecho <h3>oldest mxid per database:</h3>
SELECT datname database_name , mxid_age(datminmxid) oldest_mxid FROM pg_database order by 2 desc;

\qecho <h3>autovacuum_multixact_freeze_max_age parameter value:</h3>

select setting AS autovacuum_multixact_freeze_max_age FROM pg_catalog.pg_settings WHERE name = 'autovacuum_multixact_freeze_max_age';

\qecho <h3>Top-20 tables order by MXID age:</h3>


select relname as table_name ,mxid_age(relminmxid) mmxid_age from pg_class where relname not like 'pg_toast%'
and relminmxid::text::int>0
order by 2 desc limit 20;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - Large_objects                                    -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="Large_objects"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Large objects</b></font><hr align="left" width="460">

\qecho <br>
\qecho <details>
\qecho <h4>Note:The catalog pg_largeobject_metadata holds metadata associated with large objects. The actual large object data is stored in pg_largeobject</h4>
\qecho <br>
\qecho <h3>Number of Large objects in pg_largeobject_metadata table: </h3>
select count(*)  from pg_largeobject_metadata ;

\qecho <br>
\qecho <h3>which user own the lo ? </h3>
select pg_get_userbyid(lomowner) as user_name ,count (*) as number_of_lo from pg_largeobject_metadata 
group by 1  order by 2 desc;


\qecho <br>
\qecho <h3>pg_largeobject_metadata table size: </h3>
\dt+ pg_largeobject_metadata;

\qecho <br>
--select count(*)  from pg_largeobject;
-- in RDS PG : ERROR:  permission denied for table pg_largeobject

\qecho <br>
\qecho <h3>pg_largeobject table size: </h3>
\qecho <h4>Each large object is broken into segments or pages small enough to be conveniently stored as rows in pg_largeobject. </h4>
\qecho <h4>The amount of data per page is defined to be LOBLKSIZE (which is currently BLCKSZ/4, or typically 2 kB)</h4>
\qecho <br>
\dt+ pg_largeobject;
\qecho </details>


\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - pg_shdepend                                    -                    |
-- +----------------------------------------------------------------------------+

\qecho <a name="pg_shdepend"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>pg_shdepend</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho <h4>The catalog pg_shdepend records the dependency relationships between database objects and shared objects</h4>

select d.datname as database_name, c.relname as table_name, count(*)
from pg_catalog.pg_shdepend ps, pg_class c, pg_database d
where c.oid = ps.classid
and ps.dbid = d.oid
group by d.datname,c.relname
order by 1,3 desc;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - sequences                                    -                      |
-- +----------------------------------------------------------------------------+

\qecho <a name="sequences"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Sequences</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho <h3>sequence wraparound:</h3>
\qecho <h3>Sequences with less than 10% of the remain values</h3>
--sequence wraparound
select * from 
(
select * ,(sec.max_value - coalesce(sec.last_value,0)) as remain_values ,round((((sec.max_value - coalesce(sec.last_value,0)::float)/sec.max_value::float) *100)::int,2) remain_values_pct from pg_sequences sec ) t
where remain_values_pct <= 10 
and cycle is false 
order by remain_values_pct;
\qecho <br>
\qecho <h3>All sequences:</h3>
select * ,(sec.max_value - coalesce(sec.last_value,0)) as remain_values ,round((((sec.max_value - coalesce(sec.last_value,0)::float)/sec.max_value::float) *100)::int,2) remain_values_pct from pg_sequences sec order by remain_values_pct;
\qecho </details>


\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>



-- +----------------------------------------------------------------------------+
-- |      - functions_statistics                             -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="functions_statistics"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Functions statistics</b></font><hr align="left" width="460">
\qecho <br>
\qecho <p>The pg_stat_user_functions view will contain one row for each tracked function, showing statistics about executions of that function. </p>
\qecho <p> The track_functions parameter controls exactly which functions are tracked. </p>
\qecho <p>track_functions parameter Enables tracking of function call counts and time used. </p>
\qecho <p>Specify pl to track only procedural-language functions, all to also track SQL and C language functions. </p>
\qecho <details>
SELECT name,setting from pg_settings where name ='track_functions';
\qecho <br>
select
schemaname||'.'||funcname func_name, calls, total_time,
round((total_time/calls)::numeric,2) as mean_time, self_time
from pg_catalog.pg_stat_user_functions
order by total_time desc;

\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>




-- +----------------------------------------------------------------------------+
-- |      - triggers                                         -                  |
-- +----------------------------------------------------------------------------+

\qecho <a name="triggers"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>Triggers</b></font><hr align="left" width="460">
\qecho <br>
\qecho <details>
\qecho <h3>Trigger status/count:</h3>
SELECT
CASE pg_trigger.tgenabled 
       WHEN 'O' THEN 'trigger fires while session_replication_role set to "origin" and "local"'
   WHEN 'D' THEN 'disabled'
   WHEN 'R' THEN 'trigger fires while session_replication_role set to replica'
   WHEN 'A' THEN 'trigger fires always'
   END as  trigger_status , count (*)
   from  pg_trigger 
   group by 1;
\qecho <br>

\qecho <h3>information_schema.triggers view:</h3> 
SELECT event_object_schema as schema,
 event_object_table as table_name,
trigger_schema,
 trigger_name,
string_agg(event_manipulation, ',') as event,
        action_timing as activation,
        action_condition as condition,
        action_statement as definition
 from information_schema.triggers
 group by 1,2,3,4,6,7,8
 order by schema,
          table_name;
\qecho <br>

\qecho <h3>Triggers created by users (not internally generated):</h3>
select 
pg_trigger.tgrelid as table_id, pg_class.relname as table_name,
pg_trigger.tgname as trigger_name , 
pg_trigger.tgfoid as function_id ,
pg_proc.proname as function_name , 
pg_trigger.tgenabled as trigger_status_code ,
CASE pg_trigger.tgenabled 
       WHEN 'O' THEN 'trigger fires while session_replication_role set to "origin" and "local"'
   WHEN 'D' THEN 'disabled'
   WHEN 'R' THEN 'trigger fires while session_replication_role set to replica'
   WHEN 'A' THEN 'trigger fires always'
   END as  trigger_status , 
pg_trigger.tgisinternal is_internal_trigger ,  
pg_trigger.tgconstraint as constraint_associated_with_trigger
from  pg_trigger, pg_proc ,  pg_class
where pg_trigger.tgfoid= pg_proc.oid
and   pg_trigger.tgrelid = pg_class.oid
and pg_trigger.tgisinternal is false
order by table_id , trigger_status_code;
\qecho <br>

\qecho <h3>Triggers internally generated (usually, to enforce the constraint identified by tgconstraint):</h3>
\qecho <h3> pg_trigger.tgisinternal is true </h3>
select 
pg_trigger.tgrelid as table_id, pg_class.relname as table_name,
pg_trigger.tgname as trigger_name , 
pg_trigger.tgfoid as function_id ,
pg_proc.proname as function_name , 
pg_trigger.tgenabled as trigger_status_code ,
CASE pg_trigger.tgenabled 
       WHEN 'O' THEN 'trigger fires while session_replication_role set to "origin" and "local"'
   WHEN 'D' THEN 'disabled'
   WHEN 'R' THEN 'trigger fires while session_replication_role set to replica'
   WHEN 'A' THEN 'trigger fires always'
   END as  trigger_status , 
pg_trigger.tgisinternal is_internal_trigger ,  
pg_trigger.tgconstraint as constraint_associated_with_trigger
from  pg_trigger, pg_proc ,  pg_class
where pg_trigger.tgfoid= pg_proc.oid
and   pg_trigger.tgrelid = pg_class.oid
and pg_trigger.tgisinternal is true
order by table_id , trigger_status_code;

\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>


-- +----------------------------------------------------------------------------+
-- |      - pg_hba.conf                                   -                     |
-- +----------------------------------------------------------------------------+


\qecho <a name="pg_hba.conf"></a>
\qecho <font size="+1" face="Arial,Helvetica,Geneva,sans-serif" color="#16191f"><b>pg_hba.conf</b></font><hr align="left" width="460">

\qecho <br>
\qecho <details>
\qecho <h4>Note: this view pg_hba_file_rules reports on the current contents of the file, not on what was last loaded by the server </h4>
select * from pg_hba_file_rules;
\qecho </details>

\qecho <center>[<a class="noLink" href="#top">Top</a>]</center><p>


\q
