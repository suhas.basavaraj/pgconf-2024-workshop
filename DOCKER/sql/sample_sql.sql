

--Table1:

CREATE TABLE IF NOT EXISTS public.rnr_expiry_date
(
    airline_iata_code character(2) COLLATE pg_catalog."default",
    pnr_number character varying(15) COLLATE pg_catalog."default" NOT NULL,
    calculated_pnr_expiry_date timestamp(0) without time zone,
    row_num bigint,
    arc_expiry_date timestamp(0) without time zone,
    status character varying(10) COLLATE pg_catalog."default"
);

insert into public.rnr_expiry_date 
select 'XQ' , upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),'2023-01-01 00:00:00',generate_series(1,1000), '2023-02-02 00:00:00' ,null;
CREATE INDEX rnr_expiry_date_idx1 ON public.rnr_expiry_date (row_num ASC NULLS LAST);
CREATE INDEX rnr_expiry_date_idx2  ON public.rnr_expiry_date (airline_iata_code COLLATE pg_catalog."default" ASC NULLS LAST, pnr_number COLLATE pg_catalog."default" ASC NULLS LAST);
CREATE INDEX rnr_expiry_date_idx3  ON public.rnr_expiry_date (pnr_number ASC NULLS LAST);

vacuum analyze public.rnr_expiry_date;

---------------
--Table2:

CREATE TABLE IF NOT EXISTS public.rnr_segment_pax
(
    airline_iata_code character varying(6) COLLATE pg_catalog."default" NOT NULL,
    pnr_number character varying(15) COLLATE pg_catalog."default" NOT NULL,
    segment_pax_id numeric(25,0) NOT NULL,
    oandd_id numeric(25,0) NOT NULL,
    segment_id numeric(25,0) NOT NULL,
    cabin_class character varying(15) COLLATE pg_catalog."default",
    pax_id numeric(25,0) NOT NULL,
    ticket_number character varying(25) COLLATE pg_catalog."default",
    ticket_type character varying(10) COLLATE pg_catalog."default",
    archive_status smallint NOT NULL DEFAULT (0)::smallint,
    certificate_number character varying(100) COLLATE pg_catalog."default",
    loyalty_number character varying(25) COLLATE pg_catalog."default",
    arc_expiry_date timestamp(0) without time zone,
    CONSTRAINT rnr_segment_pax_pk PRIMARY KEY (airline_iata_code, pnr_number, segment_id, pax_id),
    CONSTRAINT rnr_segment_pax_ck1 CHECK (ticket_type::text = ANY (ARRAY['E'::character varying::text, 'A'::character varying::text, 'C'::character varying::text, 'M'::character varying::text, 'I'::character varying::text]))
);

insert into public.rnr_segment_pax (airline_iata_code, pnr_number, segment_pax_id, oandd_id, segment_id, pax_id, ticket_type, arc_expiry_date )
select 'XQ',upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(1,1000),generate_series(1,1000),
generate_series(1,1000),generate_series(1,1000),'A','2023-01-01 00:00:00';

insert into public.rnr_segment_pax (airline_iata_code, pnr_number, segment_pax_id, oandd_id, segment_id, pax_id, ticket_type, arc_expiry_date )
select 'XQ',upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(1001,2000),generate_series(1001,2000),
generate_series(1001,2000),generate_series(1001,2000),'I','2023-01-01 00:00:00';

insert into public.rnr_segment_pax (airline_iata_code, pnr_number, segment_pax_id, oandd_id, segment_id, pax_id, ticket_type, arc_expiry_date)
select 'XQ',upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(2001,3000),generate_series(2001,3000),
generate_series(2001,3000),generate_series(2001,3000),'E','2023-01-01 00:00:00';

insert into public.rnr_segment_pax (airline_iata_code, pnr_number, segment_pax_id, oandd_id, segment_id, pax_id, ticket_type, arc_expiry_date)
select 'XQ',upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(3001,4000),generate_series(3001,4000),
generate_series(3001,4000),generate_series(3001,4000),'M','2023-01-01 00:00:00';

CREATE INDEX rnr_segment_pax_idx1
    ON public.rnr_segment_pax USING btree
    (loyalty_number COLLATE pg_catalog."default" ASC NULLS LAST, airline_iata_code COLLATE pg_catalog."default" ASC NULLS LAST, arc_expiry_date ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS rnr_segment_pax_pn_idx1
    ON public.rnr_segment_pax USING btree
    (pnr_number COLLATE pg_catalog."default" ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS rnr_segment_pax_seq_idx1
    ON public.rnr_segment_pax USING btree
    (segment_id ASC NULLS LAST);

vacuum analyze public.rnr_segment_pax;


--------------------------------------------

--Table3:

CREATE TABLE IF NOT EXISTS public.rnr_segment
(
    airline_iata_code character varying(6) COLLATE pg_catalog."default" NOT NULL,
    pnr_number character varying(15) COLLATE pg_catalog."C" NOT NULL,
    segment_id numeric(25,0) NOT NULL,
    oandd_id numeric(25,0),
    price_id numeric(25,0),
    flight_carrier character varying(6) COLLATE pg_catalog."default" ,
    flight_number integer ,
    flight_suffix character varying(1) COLLATE pg_catalog."default" ,
    flight_date_ltc timestamp(0) without time zone ,
    airline_company_code character varying(6) COLLATE pg_catalog."default",
    bd_airport_code character varying(5) COLLATE pg_catalog."default" ,
    off_airport_code character varying(5) COLLATE pg_catalog."default" ,
    segment_status character varying(50) COLLATE pg_catalog."default" ,
    flight_status character varying(30) COLLATE pg_catalog."default",
    flight_type character varying(15) COLLATE pg_catalog."default",
    cabin_class character varying(15) COLLATE pg_catalog."default",
    arc_expiry_date timestamp(0) without time zone,
    oandd_dep_date_ltc timestamp(0) without time zone,
    added_time timestamp(6) without time zone,
    dep_date_ltc timestamp(0) without time zone ,
    arr_date_utc timestamp(0) without time zone,
    dep_date_utc timestamp(0) without time zone,
    origin character varying(5) COLLATE pg_catalog."default",
    destination character varying(5) COLLATE pg_catalog."default",
    CONSTRAINT rnr_segment_pk PRIMARY KEY (pnr_number, segment_id, airline_iata_code)
);

insert into public.rnr_segment (airline_iata_code, pnr_number, segment_id, FLIGHT_CARRIER,FLIGHT_NUMBER,FLIGHT_SUFFIX,FLIGHT_DATE_LTC)
select 'XQ',
upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(1,1000),'XQ',110,'*','2023-01-01 00:00:00';

insert into public.rnr_segment (airline_iata_code, pnr_number, segment_id, FLIGHT_CARRIER, FLIGHT_NUMBER, FLIGHT_SUFFIX, FLIGHT_DATE_LTC)
select 'XQ',
upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(1001,2000),'XQ',120,'*','2023-01-01 00:00:00';

insert into public.rnr_segment (airline_iata_code, pnr_number, segment_id, FLIGHT_CARRIER, FLIGHT_NUMBER,FLIGHT_SUFFIX,FLIGHT_DATE_LTC)
select 'XQ',
upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(2001,3000),'XQ',130,'*','2023-01-01 00:00:00';

insert into public.rnr_segment (airline_iata_code, pnr_number, segment_id, FLIGHT_CARRIER,FLIGHT_NUMBER,FLIGHT_SUFFIX,FLIGHT_DATE_LTC)
select 'XQ',
upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(3001,4000),'XQ',140,'*','2023-01-01 00:00:00';

insert into public.rnr_segment (airline_iata_code, pnr_number, segment_id, FLIGHT_CARRIER, FLIGHT_NUMBER, FLIGHT_SUFFIX, FLIGHT_DATE_LTC)
select 'XQ',
upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(4001,5000),'XQ',150,'*','2023-01-01 00:00:00';

insert into public.rnr_segment (airline_iata_code, pnr_number, segment_id, FLIGHT_CARRIER, FLIGHT_NUMBER, FLIGHT_SUFFIX, FLIGHT_DATE_LTC)
select 'XQ',
upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(5001,6000),'XQ',160,'*','2023-01-01 00:00:00';


CREATE INDEX rnr_segment_idx1  ON public.rnr_segment USING btree
    (flight_date_ltc ASC NULLS LAST, bd_airport_code COLLATE pg_catalog."default" ASC NULLS LAST, off_airport_code COLLATE pg_catalog."default" ASC NULLS LAST, flight_number ASC NULLS LAST, flight_carrier COLLATE pg_catalog."default" ASC NULLS LAST, flight_suffix COLLATE pg_catalog."default" ASC NULLS LAST, airline_iata_code COLLATE pg_catalog."default" ASC NULLS LAST, arc_expiry_date ASC NULLS LAST);

CREATE INDEX rnr_segment_idx2
    ON public.rnr_segment USING btree
    (dep_date_ltc ASC NULLS LAST, flight_number ASC NULLS LAST, bd_airport_code COLLATE pg_catalog."default" ASC NULLS LAST, off_airport_code COLLATE pg_catalog."default" ASC NULLS LAST, flight_carrier COLLATE pg_catalog."default" ASC NULLS LAST, flight_suffix COLLATE pg_catalog."default" ASC NULLS LAST, arc_expiry_date ASC NULLS LAST);

CREATE INDEX rnr_segment_idx3
    ON public.rnr_segment USING btree
    (pnr_number COLLATE pg_catalog."default" ASC NULLS LAST, arr_date_utc ASC NULLS LAST, airline_iata_code COLLATE pg_catalog."default" ASC NULLS LAST, arc_expiry_date ASC NULLS LAST);

CREATE INDEX rnr_segment_idx4
    ON public.rnr_segment USING btree
    (dep_date_utc ASC NULLS LAST, added_time ASC NULLS LAST, airline_iata_code COLLATE pg_catalog."default" ASC NULLS LAST, arc_expiry_date ASC NULLS LAST);

CREATE INDEX rnr_segment_idx5
    ON public.rnr_segment USING btree
    (origin COLLATE pg_catalog."default" ASC NULLS LAST, destination COLLATE pg_catalog."default" ASC NULLS LAST, oandd_dep_date_ltc ASC NULLS LAST, airline_iata_code COLLATE pg_catalog."default" ASC NULLS LAST, arc_expiry_date ASC NULLS LAST);

CREATE INDEX rnr_segment_idx6
    ON public.rnr_segment USING btree
    (pnr_number COLLATE pg_catalog."default" ASC NULLS LAST, oandd_id ASC NULLS LAST, segment_id ASC NULLS LAST, airline_iata_code COLLATE pg_catalog."default" ASC NULLS LAST, arc_expiry_date ASC NULLS LAST);


EXPLAIN ANALYZE UPDATE public.RNR_SEGMENT_PAX x SET ARC_EXPIRY_DATE = y.ARC_EXPIRY_DATE
       FROM (SELECT AIRLINE_IATA_CODE, PNR_NUMBER, ARC_EXPIRY_DATE, 0+row_num ROW_NUM
               FROM public.RNR_EXPIRY_DATE 
              WHERE airline_iata_code = 'XQ' 
                AND row_num BETWEEN (1*5000)+0 AND (1+1)*5000) y
         WHERE  x.airline_iata_code = y.airline_iata_code 
             AND  x.PNR_NUMBER =y.PNR_NUMBER;



--------------------------------------

--Table4:

CREATE TABLE IF NOT EXISTS public.rnr_seat_numbers
(
    airline_iata_code character varying(6) COLLATE pg_catalog."default" NOT NULL,
    pnr_number character varying(15) COLLATE pg_catalog."default" NOT NULL,
    segment_id numeric(25,0) NOT NULL,
    pax_id numeric(25,0) NOT NULL,
    seat_id numeric(25,0) NOT NULL,
    bd_airport_code character varying(5) COLLATE pg_catalog."default",
    off_airport_code character varying(5) COLLATE pg_catalog."default",
    seat_number character varying(5) COLLATE pg_catalog."default",
    seat_status character varying(20) COLLATE pg_catalog."default",
    ssr_id character varying(100) COLLATE pg_catalog."default",
    archive_status smallint DEFAULT (0)::smallint,
    seat_alloc_id numeric(25,0),
    archive_date timestamp(0) without time zone,
    seat_attribute_code character varying(201) COLLATE pg_catalog."default",
    channel_code character varying(20) COLLATE pg_catalog."default",
    arc_expiry_date timestamp(0) without time zone,
    CONSTRAINT rnr_seat_numbers_pk PRIMARY KEY (pnr_number, segment_id, pax_id, seat_id, airline_iata_code)
);


insert into public.rnr_seat_numbers (pnr_number, segment_id, pax_id, seat_id, airline_iata_code)
select upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(1,1000),generate_series(1,1000),generate_series(1,1000),'XQ';

insert into public.rnr_seat_numbers (pnr_number, segment_id, pax_id, seat_id, airline_iata_code)
select upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(1001,2000),generate_series(1001,2000),generate_series(1001,2000),'XQ';

insert into public.rnr_seat_numbers (pnr_number, segment_id, pax_id, seat_id, airline_iata_code)
select upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(2001,3000),generate_series(2001,3000),generate_series(2001,3000),'XQ';

insert into public.rnr_seat_numbers (pnr_number, segment_id, pax_id, seat_id, airline_iata_code)
select upper(substring(concat(md5(random()::text), md5(random()::text)), 0, 7)),
generate_series(3001,4000),generate_series(3001,4000),generate_series(3001,4000),'XQ';

vacuum Analyze public.rnr_seat_numbers;

--Table6:
CREATE TABLE IF NOT EXISTS public.oiltype
(
    oiltype_id bigint NOT NULL,
    descr character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT oiltype_pkey PRIMARY KEY (oiltype_id)
);

CREATE INDEX IF NOT EXISTS oiltype_oiltyp_in
    ON public.oiltype USING btree
    (oiltype_id ASC NULLS LAST);
	
	

--Table5:
CREATE TABLE IF NOT EXISTS public.test_veh
(
    test_veh_id bigint NOT NULL,
    oiltype_id bigint,
    vehicle_id character varying(50) COLLATE pg_catalog."default",
    serviceprogram_id character varying(100) COLLATE pg_catalog."default",
    startdate timestamp without time zone,
    enddate timestamp without time zone,
    last_update_dt timestamp without time zone,
    CONSTRAINT test_veh_pkey PRIMARY KEY (test_veh_id),
    CONSTRAINT test_veh_oiltype_id_fkey FOREIGN KEY (oiltype_id)
        REFERENCES public.oiltype (oiltype_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT test_veh_oiltype_id_fkey1 FOREIGN KEY (oiltype_id)
        REFERENCES public.oiltype (oiltype_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE INDEX IF NOT EXISTS test_veh_enddate_ind
    ON public.test_veh USING btree
    (enddate ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS test_veh_oiltype_id_ind
    ON public.test_veh USING btree
    (oiltype_id ASC NULLS LAST);



	
--Table7:	
CREATE TABLE IF NOT EXISTS public.serviceprogram
(
    serial bigint NOT NULL,
    serviceprogram_id character varying(50) COLLATE pg_catalog."default",
    progname character varying(150) COLLATE pg_catalog."default",
    CONSTRAINT serviceprogram_pkey PRIMARY KEY (serial)
);

CREATE INDEX IF NOT EXISTS progname_id_ind
    ON public.serviceprogram USING btree
    (progname COLLATE pg_catalog."default" ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS serviceprogram_id_ind
    ON public.serviceprogram USING btree
    (serviceprogram_id COLLATE pg_catalog."default" ASC NULLS LAST);
	
--Table8:
CREATE TABLE IF NOT EXISTS public.vehicleservicehistory
(
    v_id bigint NOT NULL,
    test_veh_id bigint,
    desc_1 character varying(50) COLLATE pg_catalog."default",
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    CONSTRAINT vehicleservicehistory_pkey PRIMARY KEY (v_id)
);

CREATE INDEX IF NOT EXISTS veh_end_date_id_ind
    ON public.vehicleservicehistory USING btree
    (end_date ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS veh_ser_ind
    ON public.vehicleservicehistory USING btree
    (test_veh_id ASC NULLS LAST);

CREATE INDEX IF NOT EXISTS vehicleservicehistory_v_id_ind
    ON public.vehicleservicehistory USING btree
    (test_veh_id ASC NULLS LAST);


--Function creation 
CREATE OR REPLACE FUNCTION public.return_data()
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
return 'EE9F41' ;
END;
$BODY$;


--plprofiler


create schema plp;
CREATE TABLE plp.rental (
    rental_id integer NOT NULL PRIMARY KEY,
    rental_date timestamp with time zone NOT NULL,
    inventory_id integer NOT NULL,
    customer_id integer NOT NULL,
    return_date timestamp with time zone,
    staff_id integer NOT NULL,
    last_update timestamp with time zone DEFAULT now() NOT NULL
);

CREATE TABLE plp.inventory (
    inventory_id integer  NOT NULL,
    film_id integer NOT NULL,
    store_id integer NOT NULL,
    last_update timestamp with time zone DEFAULT now() NOT NULL
);


INSERT INTO plp.rental VALUES (
generate_series(1,100000), 
now()-random()*(now() - (now() - '2 years'::interval)),
generate_series(1,100000),
random()*generate_series(1,100000),
now()-random()*(now() - (now() - '2 years'::interval)),
random()*generate_series(1,100000),
now()-random()*(now() - (now() - '2 years'::interval))
);

INSERT INTO plp.inventory VALUES (
generate_series(1,100000),
random()*generate_series(1,100000),
random()*generate_series(1,100000),
now()-random()*(now() - (now() - '2 years'::interval))
);


CREATE OR REPLACE FUNCTION plp.inventory_in_stock(p_inventory_id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_rentals INTEGER;
    v_out     INTEGER;
BEGIN
    -- AN ITEM IS IN-STOCK IF THERE ARE EITHER NO ROWS IN THE rental TABLE
    -- FOR THE ITEM OR ALL ROWS HAVE return_date POPULATED
IF NOT EXISTS (SELECT 1 FROM plp.rental WHERE inventory_id =    p_inventory_id) THEN
     RETURN TRUE;
     END IF;

    SELECT COUNT(rental_id) INTO v_out
    FROM plp.rental WHERE EXISTS (SELECT 1 FROM plp.inventory
    WHERE inventory.inventory_id = p_inventory_id)
    AND rental.return_date IS NULL;
    IF v_out > 0 THEN
      RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
END $$;

--Anazlyze

create table t1 ( a int, b int, c int );
alter table t1 set (autovacuum_enabled=false);
insert into t1 values (generate_series(1,100000),generate_series(1,100000),generate_series(1,100000));
create index i2 on t1 (b,a);

--Index_Bloat:

Create table index_bloat_1(id int ,salary money,  name varchar , address varchar);
insert into index_bloat_1 values( generate_series(1,100000),generate_series(1,100000),'amazon','Financial district');
create index index_bloat_1_idx on index_bloat_1(id);
create index index_bloat_2_idx on index_bloat_1(id,salary);


-- pgpartman


CREATE TABLE partition_table (
     id bigint not null
    , name text
    , tab_dt timestamptz DEFAULT now() not null
    , address text)
PARTITION BY RANGE (id);

CREATE INDEX ON partition_table (id);

CREATE TABLE nonpartition_table (
     id bigint not null
    , name text
    , tab_dt timestamptz DEFAULT now() not null
    , address text);

CREATE INDEX ON nonpartition_table (id);
