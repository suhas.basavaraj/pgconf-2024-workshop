CREATE USER replication_user WITH REPLICATION ENCRYPTED PASSWORD 'replicationpgc0nf';
SELECT pg_create_physical_replication_slot('repl_slot');
CREATE EXTENSION pg_repack;
CREATE EXTENSION pg_partman;
CREATE EXTENSION plprofiler;
CREATE EXTENSION pg_stat_statements;
CREATE EXTENSION pgaudit;
